package org.feng.navigation.rest.gatewayimpl.qyapi;

import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.domain.rpc.qyapi.domainobject.BaseEnterpriseWeChatRobotRequest;
import org.feng.navigation.domain.rpc.qyapi.domainobject.EnterpriseWechatRobotMarkdownRequest;
import org.feng.navigation.domain.rpc.qyapi.domainobject.EnterpriseWechatRobotTextRequest;
import org.feng.navigation.domain.rpc.qyapi.gateway.EnterpriseWechatRobotHttpClient;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月27日 14时46分
 */
@Slf4j
@Component
public class EnterpriseWechatRobotHttpClientImpl implements EnterpriseWechatRobotHttpClient {

    private static final HttpClient HTTP_CLIENT;

    static {
        HTTP_CLIENT = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .connectTimeout(Duration.of(10, ChronoUnit.SECONDS))
                .build();
    }

    public void send(BaseEnterpriseWeChatRobotRequest request) {
        String requestBody = request.toJson();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(ROBOT_URL + request.getKey()))
                .timeout(Duration.ofSeconds(20))
                .header("Content-type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody, Charset.defaultCharset()))
                .build();

        log.debug("请求参数：{}", requestBody);
        CompletableFuture<HttpResponse<String>> sendAsyncResponse = HTTP_CLIENT.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString(Charset.defaultCharset()));
        sendAsyncResponse.thenAccept(res -> log.debug("响应结果：{}", res))
                .join();
    }

/*     public static void main(String[] args) {
        EnterpriseWechatRobotHttpClient client = new EnterpriseWechatRobotHttpClientImpl();
        // 发送文本
        EnterpriseWechatRobotTextRequest request = new EnterpriseWechatRobotTextRequest();
        EnterpriseWechatRobotTextRequest.TextBody textBody = new EnterpriseWechatRobotTextRequest.TextBody().setContent("测试一下，啊哈哈哈666");
        request.setText(textBody);
        request.setKey("你自己的机器人key");
        client.send(request);

        // 发送markdown
        EnterpriseWechatRobotMarkdownRequest request1 = new EnterpriseWechatRobotMarkdownRequest();
        EnterpriseWechatRobotMarkdownRequest.MarkdownBody markdownBody = new EnterpriseWechatRobotMarkdownRequest.MarkdownBody()
                .setContent("## 实时新增用户反馈<font color=\\\"warning\\\">132例</font>，请相关同事注意\n" +
                        "         >**类型**:<font color=\\\"info\\\">用户反馈</font>\n" +
                        "         >普通用户反馈:<font color=\\\"comment\\\">117例</font>\n" +
                        "         >`VIP`用户反馈:<font color=\\\"warning\\\">15例</font>\n" +
                        "[这是一个链接](http://work.weixin.qq.com/api/doc)\n" + "[这是第二个链接](https://developer.work.weixin.qq.com/document/path/91770)");
        request1.setMarkdown(markdownBody);
        request1.setKey("你自己的机器人key");
        client.send(request1);
    } */
}
