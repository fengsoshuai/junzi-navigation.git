package org.feng.navigation.rest.gatewayimpl.tianxing;

import org.feng.navigation.client.dto.tianxing.TianXingAllNewsRequestDTO;
import org.feng.navigation.client.dto.tianxing.TianXingWeatherSearchResponseDTO;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingRequest;
import org.feng.navigation.domain.tianxing.entity.TianXingAllNewsObject;
import org.feng.navigation.domain.tianxing.entity.TianXingWeatherSearchResultData;
import org.feng.navigation.domain.tianxing.enums.TianXingAllNewsTypeEnum;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.util.MultiValueMap;

import javax.annotation.Resource;

/**
 * 天行数据转换器
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 14时45分
 */
@Mapper(componentModel = "spring", imports = {TianXingAllNewsTypeEnum.class})
public abstract class TianXingConvert {

    @Resource
    private TianXingRequestConvertComponent tianXingRequestConvertComponent;

    public MultiValueMap<String, Object> convertRequest(BaseTianXingRequest<?> request) {
        return tianXingRequestConvertComponent.execute(request, MultiValueMap.class);
    }

    /**
     * 天行新闻请求体转换为领域对象
     *
     * @param requestDTO 请求体
     * @return 天行新闻领域对象
     */
    @Mappings({
            @Mapping(target = "newsCount", source = "newsCount"),
            @Mapping(target = "tianXingAllNewsTypeEnum", expression = "java(TianXingAllNewsTypeEnum.of(requestDTO.getChannelId()))"),
    })
    @BeanMapping(ignoreByDefault = true)
    public abstract TianXingAllNewsObject transferAllNewsDomainObject(TianXingAllNewsRequestDTO requestDTO);

    /**
     * 天行天气预报响应结果转换
     *
     * @param responseData 领域响应数据
     * @return 天气预报响应
     */
    public abstract TianXingWeatherSearchResponseDTO transferToTianXingWeatherSearchResponseDTO(TianXingWeatherSearchResultData responseData);
}
