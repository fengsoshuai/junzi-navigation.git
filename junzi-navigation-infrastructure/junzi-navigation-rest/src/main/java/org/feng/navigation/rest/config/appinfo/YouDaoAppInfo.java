package org.feng.navigation.rest.config.appinfo;

import lombok.Data;

/**
 * 有道-app信息（appKey、appSecret）
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月10日 14时27分
 */
@Data
public class YouDaoAppInfo {
    private String appKey;
    private String appSecret;
}
