package org.feng.navigation.rest.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

/**
 * Http请求URL相关配置
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月10日 14时18分
 */
@Data
@Slf4j
@ConfigurationProperties(prefix = HttpUrlConfig.PREFIX)
public class HttpUrlConfig {
    public static final String PREFIX = "junzi.nav.url";

    /**
     * 有道翻译
     */
    private String youDao;

    /**
     * <a href="https://apis.tianapi.com/">天行</a>
     */
    private String tianXing;

    @PostConstruct
    private void initLog() {
        log.info("有道智云 URL：{}", youDao);
        log.info("天行数据 URL：{}", tianXing);
    }
}
