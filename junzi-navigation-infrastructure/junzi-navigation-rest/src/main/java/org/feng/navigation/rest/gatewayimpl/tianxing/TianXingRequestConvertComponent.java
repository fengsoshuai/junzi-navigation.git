package org.feng.navigation.rest.gatewayimpl.tianxing;

import cn.hutool.core.util.StrUtil;
import org.feng.navigation.common.util.GsonUtil;
import org.feng.navigation.domain.router.Router;
import org.feng.navigation.domain.rpc.tianxing.domainobject.allnews.TianXingAllNewsRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.area.TianXingAreaSearchRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.robot.TianXingRobotRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.sentence.TianXingOneSentenceEverydayRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.weather.TianXingWeatherSearchRequest;
import org.feng.navigation.domain.router.AbstractRouterFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;

/**
 * 天行请求体转换组件
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 17时15分
 */
@Component
public class TianXingRequestConvertComponent extends AbstractRouterFactory {

    @Router(classRouterKey = TianXingAllNewsRequest.class, desc = "天行数据-分类新闻接口请求体转换")
    protected MultiValueMap<String, Object> convertAllNewsRequest(TianXingAllNewsRequest request) {
        String json = GsonUtil.toJson(request);
        Map<String, Object> stringObjectMap = GsonUtil.parseToObjectMap(json);
        Map<String, List<Object>> multiMap = new HashMap<>(16);
        stringObjectMap.forEach((key, value) -> {
            ArrayList<Object> arrayList = new ArrayList<>();
            arrayList.add(value);
            multiMap.put(key, arrayList);
        });
        return new LinkedMultiValueMap<>(multiMap);
    }

    @Router(classRouterKey = TianXingAreaSearchRequest.class, desc = "天行数据-地区查询接口请求体转换")
    protected MultiValueMap<String, Object> convertAreaSearchRequest(TianXingAreaSearchRequest request) {
        MultiValueMap<String, Object> multiMap = new LinkedMultiValueMap<>();
        multiMap.put("area", Collections.singletonList(request.getArea()));
        multiMap.put("key", Collections.singletonList(request.getKey()));
        return multiMap;
    }

    @Router(classRouterKey = TianXingWeatherSearchRequest.class, desc = "天行数据-天气预报接口请求体转换")
    protected MultiValueMap<String, Object> convertWeatherSearchRequest(TianXingWeatherSearchRequest request) {
        MultiValueMap<String, Object> multiMap = new LinkedMultiValueMap<>();
        multiMap.put("city", Collections.singletonList(request.getCity()));
        multiMap.put("type", Collections.singletonList(request.getType()));
        multiMap.put("key", Collections.singletonList(request.getKey()));
        return multiMap;
    }

    @Router(classRouterKey = TianXingOneSentenceEverydayRequest.class, desc = "天行数据-每日一句请求体转换")
    protected MultiValueMap<String, Object> convertOneSentenceEverydayRequest(TianXingOneSentenceEverydayRequest request) {
        MultiValueMap<String, Object> multiMap = new LinkedMultiValueMap<>();
        multiMap.put("rand", Collections.singletonList(request.getRand()));
        multiMap.put("date", Collections.singletonList(request.getDate()));
        multiMap.put("key", Collections.singletonList(request.getKey()));
        return multiMap;
    }
    @Router(classRouterKey = TianXingRobotRequest.class, desc = "天行数据-机器人请求体转换")
    protected MultiValueMap<String, Object> convertOneSentenceEverydayRequest(TianXingRobotRequest request) {
        MultiValueMap<String, Object> multiMap = new LinkedMultiValueMap<>();
        multiMap.put("question", Collections.singletonList(request.getQuestion()));
        multiMap.put("key", Collections.singletonList(request.getKey()));
        if(StrUtil.isNotEmpty(request.getUniqueId())){
            multiMap.put("uniqueid", Collections.singletonList(request.getUniqueId()));
        }
        return multiMap;
    }
}
