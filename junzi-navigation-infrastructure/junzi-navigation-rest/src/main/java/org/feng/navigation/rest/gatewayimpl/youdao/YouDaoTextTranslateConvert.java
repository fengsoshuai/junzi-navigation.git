package org.feng.navigation.rest.gatewayimpl.youdao;

import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateRequestDTO;
import org.feng.navigation.domain.youdao.entity.TextTranslateObject;
import org.feng.navigation.domain.youdao.enums.TextTranslateSupportLanguageEnum;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * 有道文本翻译实体转换器
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 22时18分
 */
@Mapper(componentModel = "spring", imports = {TextTranslateSupportLanguageEnum.class})
public interface YouDaoTextTranslateConvert {

    /**
     * 从请求体转换为文本翻译领域对象
     *
     * @param dto 请求体DTO
     * @return 文本翻译领域对象
     */
    @Mappings({
            @Mapping(target = "from", expression = "java(TextTranslateSupportLanguageEnum.of(dto.getFrom()))"),
            @Mapping(target = "to", expression = "java(TextTranslateSupportLanguageEnum.of(dto.getTo()))"),
            @Mapping(target = "itemText", source = "dto.text")
    })
    @BeanMapping(ignoreByDefault = true)
    TextTranslateObject transferToDomainObject(YouDaoTextTranslateRequestDTO dto);
}
