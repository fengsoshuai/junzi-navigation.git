package org.feng.navigation.rest.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.rest.config.appinfo.TianXingAppInfo;
import org.feng.navigation.rest.config.appinfo.YouDaoAppInfo;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

/**
 * 应用信息配置：对接的接口对应的token、key/secret
 * <ul><b>目前配置的有：</b>
 *     <li>有道</li>
 *     <li>天行数据</li>
 * </ul>
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月10日 14时26分
 */
@Data
@Slf4j
@ConfigurationProperties(prefix = AppInfoConfig.PREFIX)
public class AppInfoConfig {
    public static final String PREFIX = "junzi.nav.app";

    private YouDaoAppInfo youDao;

    private TianXingAppInfo tianXing;

    @PostConstruct
    private void initLog() {
        log.info("有道智云 key 和 secret：{}", youDao);
        log.info("天行数据 key ：{}", tianXing);
    }
}
