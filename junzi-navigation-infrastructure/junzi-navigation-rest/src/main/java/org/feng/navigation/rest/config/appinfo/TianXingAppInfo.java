package org.feng.navigation.rest.config.appinfo;

import lombok.Data;

/**
 * <a href="https://www.tianapi.com/list/">天行数据</a>接口,app信息（appKey） <br>
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 13时32分
 */
@Data
public class TianXingAppInfo {
    private String appKey;
}
