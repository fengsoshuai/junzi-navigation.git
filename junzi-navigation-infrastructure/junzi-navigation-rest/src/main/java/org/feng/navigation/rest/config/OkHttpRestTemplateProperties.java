package org.feng.navigation.rest.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * OkHttp 属性配置
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月16日 14时06分
 */
@Data
@Slf4j
@ConfigurationProperties(prefix = OkHttpRestTemplateProperties.PREFIX)
public class OkHttpRestTemplateProperties {
    public static final String PREFIX = "junzi.okhttp";
    /**
     * 读取超时
     */
    private Integer readTimeout = 30;
    /**
     * 写入超时
     */
    private Integer writeTimeout = 30;
    /**
     * 连接超时时间
     */
    private Integer connectTimeout = 30;
    /**
     * 最大空闲连接数
     */
    private Integer maxIdleConnections = 10;
    /**
     * 空闲连接存活时间
     */
    private Integer keepAliveDuration = 5;
}
