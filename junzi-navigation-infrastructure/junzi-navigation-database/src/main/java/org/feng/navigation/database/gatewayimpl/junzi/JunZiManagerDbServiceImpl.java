package org.feng.navigation.database.gatewayimpl.junzi;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.feng.navigation.database.entity.JunZiManagerDO;
import org.feng.navigation.database.mapper.JunZiManagerMapper;
import org.feng.navigation.domain.junzi.domainobject.JunZiManagerObject;
import org.feng.navigation.domain.junzi.gateway.JunZiManagerDbService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月19日 12时27分
 */
@Service
public class JunZiManagerDbServiceImpl extends ServiceImpl<JunZiManagerMapper, JunZiManagerDO> implements JunZiManagerDbService {
    @Resource
    private JunZiManagerMapper junZiManagerMapper;

    @Resource
    private JunZiManagerConvert junZiManagerConvert;

    @Override
    public List<JunZiManagerObject> listJunZiManager() {
        List<JunZiManagerDO> junZiManagerDOList = junZiManagerMapper.listJunZiManager();

        if (CollectionUtils.isEmpty(junZiManagerDOList)) {
            return Collections.emptyList();
        }

        return junZiManagerConvert.transferToDomainObjectList(junZiManagerDOList);
    }
}
