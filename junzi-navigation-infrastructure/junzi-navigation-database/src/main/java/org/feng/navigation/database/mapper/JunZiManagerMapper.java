package org.feng.navigation.database.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.feng.navigation.database.entity.JunZiManagerDO;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月13日 14时18分
 */
@Mapper
@Component
public interface JunZiManagerMapper extends BaseMapper<JunZiManagerDO> {
    @Select("select * from junzi_manager limit 5")
    List<JunZiManagerDO> listJunZiManager();
}
