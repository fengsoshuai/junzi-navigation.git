package org.feng.navigation.database.gatewayimpl.junzi;

import org.feng.navigation.client.dto.junzi.JunZiAddCollectedSiteRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiListCollectedSiteResponseDTO;
import org.feng.navigation.common.util.GroupByUtil;
import org.feng.navigation.database.entity.JunZiCollectedSiteDO;
import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteObject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.ArrayList;
import java.util.List;

/**
 * 站点数据对象转换
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 20时30分
 */
@Mapper(componentModel = "spring")
public interface JunZiCollectedSiteConvert {

    /**
     * 领域对象转换为数据对象
     *
     * @param junZiCollectedSiteDO 数据对象
     * @return 站点领域对象
     */
    JunZiCollectedSiteObject transferToDomainObject(JunZiCollectedSiteDO junZiCollectedSiteDO);

    /**
     * 领域对象转换为数据对象
     *
     * @param junZiCollectedSiteObject 站点领域对象
     * @return 数据对象
     */
    @Mappings({
            @Mapping(target = "createdTime", ignore = true),
            @Mapping(target = "updateTime", ignore = true),
            @Mapping(target = "id", ignore = true),
    })
    JunZiCollectedSiteDO transferToDo(JunZiCollectedSiteObject junZiCollectedSiteObject);

    JunZiCollectedSiteObject addDtoToDomainObject(JunZiAddCollectedSiteRequestDTO requestDTO);

    @Mappings({
            @Mapping(target = "siteType", source = "junZiCollectedSiteObject.junZiCollectedSiteTypeObject.type")
    })
    JunZiListCollectedSiteResponseDTO.JunZiListCollectedSiteData transferToListDtoData(JunZiCollectedSiteObject junZiCollectedSiteObject);

    List<JunZiListCollectedSiteResponseDTO.JunZiListCollectedSiteData> transferToListDtoDataList(List<JunZiCollectedSiteObject> junZiCollectedSiteObjects);

    default List<JunZiListCollectedSiteResponseDTO.JunZiListCollectedSiteDataBody> transferToListDto(List<JunZiCollectedSiteObject> junZiCollectedSiteObjects) {
        List<JunZiListCollectedSiteResponseDTO.JunZiListCollectedSiteData> junZiListCollectedSiteDataList = transferToListDtoDataList(junZiCollectedSiteObjects);
        List<JunZiListCollectedSiteResponseDTO.JunZiListCollectedSiteDataBody> bodyList = new ArrayList<>();
        GroupByUtil.groupingBySingleComparableParam(junZiListCollectedSiteDataList, JunZiListCollectedSiteResponseDTO.JunZiListCollectedSiteData::getSiteType).forEach((type, dataList) -> {
            JunZiListCollectedSiteResponseDTO.JunZiListCollectedSiteDataBody body = new JunZiListCollectedSiteResponseDTO.JunZiListCollectedSiteDataBody();
            body.setSiteType(type);
            body.setDataList(dataList);
            bodyList.add(body);
        });
        return bodyList;
    }
}
