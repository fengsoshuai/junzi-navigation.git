package org.feng.navigation.database.gatewayimpl.junzi;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.feng.navigation.database.entity.JunZiCollectedSiteTypeDO;
import org.feng.navigation.database.mapper.JunZiCollectedSiteTypeMapper;
import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteTypeObject;
import org.feng.navigation.domain.junzi.gateway.JunZiCollectedSiteTypeDbService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO 类的描述
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 20时58分
 */
@Service
public class JunZiCollectedSiteTypeDbServiceImpl extends ServiceImpl<JunZiCollectedSiteTypeMapper, JunZiCollectedSiteTypeDO> implements JunZiCollectedSiteTypeDbService {

    @Resource
    private JunZiCollectedSiteTypeConvert junZiCollectedSiteTypeConvert;

    public List<JunZiCollectedSiteTypeObject> listJunZiCollectedSiteTypes() {
        return super.list().stream()
                .map(junZiCollectedSiteTypeDO -> junZiCollectedSiteTypeConvert.transferToDomainObject(junZiCollectedSiteTypeDO))
                .collect(Collectors.toList());
    }
}
