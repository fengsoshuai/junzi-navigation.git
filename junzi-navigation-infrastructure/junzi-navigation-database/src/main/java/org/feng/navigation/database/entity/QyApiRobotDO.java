package org.feng.navigation.database.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 企业api机器人对应的表
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 16时55分
 */
@Data
@TableName("qyapi_robot")
public class QyApiRobotDO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 群名+机器人名
     */
    private String robotName;
    /**
     * 机器人key
     */
    private String robotKey;
}
