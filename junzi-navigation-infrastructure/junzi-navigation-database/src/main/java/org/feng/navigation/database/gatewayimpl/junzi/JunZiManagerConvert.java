package org.feng.navigation.database.gatewayimpl.junzi;

import org.feng.navigation.client.dto.junzi.JunZiManagerDTO;
import org.feng.navigation.database.entity.JunZiManagerDO;
import org.feng.navigation.domain.junzi.domainobject.JunZiManagerObject;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

/**
 * {@link JunZiManagerDO} 相关的实例转换
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月13日 16时39分
 */
@Mapper(componentModel = "spring")
public interface JunZiManagerConvert {

    /**
     * 转换实体：{@link JunZiManagerDO} 转换为 {@link JunZiManagerObject}
     *
     * @param junZiManagerDO {@link JunZiManagerDO}实例
     * @return {@link JunZiManagerObject} 实例
     */
    @Mappings({
            @Mapping(target = "junziId", source = "junziId"),
            @Mapping(target = "name", source = "name"),
            @Mapping(target = "loginName", source = "loginName"),
            @Mapping(target = "createDatetime", source = "createDatetime"),
    })
    @BeanMapping(ignoreByDefault = true)
    JunZiManagerObject transferToDomainObject(JunZiManagerDO junZiManagerDO);

    /**
     * 转换实体列表：{@link JunZiManagerDO} 转换为 {@link JunZiManagerObject}
     *
     * @param junZiManagerDOList {@link JunZiManagerDO} 实例列表
     * @return {@link JunZiManagerObject} 实例列表
     */
    List<JunZiManagerObject> transferToDomainObjectList(List<JunZiManagerDO> junZiManagerDOList);

    @Mappings({
            @Mapping(target = "junziId", source = "junziId"),
            @Mapping(target = "name", source = "name"),
            @Mapping(target = "loginName", source = "loginName"),
            @Mapping(target = "createDatetime", source = "createDatetime"),
    })
    JunZiManagerDTO transferToDto(JunZiManagerObject junZiManagerObject);

    List<JunZiManagerDTO> transferToDtoList(List<JunZiManagerObject> junZiManagerObjectList);
}
