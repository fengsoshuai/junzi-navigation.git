package org.feng.navigation.database.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.feng.navigation.database.entity.JunZiCollectedSiteDO;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 09时29分
 */
@Mapper
@Component
public interface JunZiCollectedSiteMapper extends BaseMapper<JunZiCollectedSiteDO> {
    @Select("select site.id, site.`name`, type.type as siteType, site.ico, site.url, site.remark, site.created_time,site.update_time from collected_site as site, collected_site_type as type where site.site_type = type.id")
    List<JunZiCollectedSiteDO> listCollectedSites();

    @Select("select site.id, site.`name`, type.type  as siteType, site.ico, site.url, site.remark, site.created_time,site.update_time from collected_site as site, collected_site_type as type where site.site_type = type.id and type.id = #{siteType}")
    List<JunZiCollectedSiteDO> listCollectedSitesByType(@Param("siteType") Integer siteType);

    @Select("select site.id, site.`name`, type.type  as siteType, site.ico, site.url, site.remark, site.created_time,site.update_time from collected_site as site, collected_site_type as type where site.site_type = type.id and type.id in (#{siteTypes})")
    List<JunZiCollectedSiteDO> listCollectedSitesByTypes(@Param("siteTypes") String siteTypes);
}
