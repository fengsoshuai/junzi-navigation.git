package org.feng.navigation.database.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 09时25分
 */
@Data
@TableName("collected_site")
public class JunZiCollectedSiteDO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer siteType;
    private String url;
    private String ico;
    private String logo;
    private String remark;
    private LocalDateTime createdTime;
    private LocalDateTime updateTime;
}
