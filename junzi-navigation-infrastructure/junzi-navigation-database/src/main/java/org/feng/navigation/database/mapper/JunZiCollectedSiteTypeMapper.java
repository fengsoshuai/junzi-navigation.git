package org.feng.navigation.database.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.feng.navigation.database.entity.JunZiCollectedSiteTypeDO;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 09时42分
 */
@Mapper
@Component
public interface JunZiCollectedSiteTypeMapper extends BaseMapper<JunZiCollectedSiteTypeDO> {
}
