/**
 * 基础架构层的数据库模块：主要是对mysql数据库进行操作；包含Mapper、Service实现
 *
 * @author junzi
 */
package org.feng.navigation.database;