package org.feng.navigation.database.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.feng.navigation.database.entity.QyApiRobotDO;
import org.springframework.stereotype.Component;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 16时59分
 */
@Mapper
@Component
public interface QyApiRobotMapper extends BaseMapper<QyApiRobotDO> {
}
