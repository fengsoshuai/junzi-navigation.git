package org.feng.navigation.database.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 菌子管理者对应的数据库表实体
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月13日 14时14分
 */
@Data
@TableName("junzi_manager")
public class JunZiManagerDO {
    @TableId(type = IdType.AUTO)
    private Integer junziId;
    private String name;
    private String loginName;
    private String loginPwd;
    private LocalDateTime createDatetime;
    private LocalDateTime updateDatetime;

}
