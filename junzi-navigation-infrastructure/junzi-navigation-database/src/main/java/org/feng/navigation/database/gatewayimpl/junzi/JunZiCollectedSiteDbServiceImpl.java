package org.feng.navigation.database.gatewayimpl.junzi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.database.entity.JunZiCollectedSiteDO;
import org.feng.navigation.database.mapper.JunZiCollectedSiteMapper;
import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteObject;
import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteTypeObject;
import org.feng.navigation.domain.junzi.gateway.JunZiCollectedSiteDbService;
import org.feng.navigation.domain.junzi.gateway.JunZiCollectedSiteTypeDbService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 站点数据库服务实现
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 09时47分
 */
@Slf4j
@Service
public class JunZiCollectedSiteDbServiceImpl extends ServiceImpl<JunZiCollectedSiteMapper, JunZiCollectedSiteDO> implements JunZiCollectedSiteDbService {

    @Resource
    private JunZiCollectedSiteConvert junZiCollectedSiteConvert;
    @Resource
    private JunZiCollectedSiteTypeDbService junZiCollectedSiteTypeDbService;

    public List<JunZiCollectedSiteObject> listJunZiCollectedSites(int current, int size) {
        // 查找站点类型
        Map<Integer, JunZiCollectedSiteTypeObject> typeObjectMap = junZiCollectedSiteTypeDbService.listJunZiCollectedSiteTypes().stream()
                .collect(Collectors.toMap(JunZiCollectedSiteTypeObject::getId, Function.identity()));

        QueryWrapper<JunZiCollectedSiteDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("update_time").orderByAsc("id");
        return super.page(new Page<>(current, size), queryWrapper).getRecords().stream()
                .map(junZiCollectedSiteDO -> {
                    // 数据对象转换为领域对象
                    JunZiCollectedSiteObject junZiCollectedSiteObject = junZiCollectedSiteConvert.transferToDomainObject(junZiCollectedSiteDO);
                    // 查找到站点类型
                    JunZiCollectedSiteTypeObject junZiCollectedSiteTypeObject = typeObjectMap.get(junZiCollectedSiteObject.getSiteType());
                    junZiCollectedSiteObject.setJunZiCollectedSiteTypeObject(junZiCollectedSiteTypeObject);
                    return junZiCollectedSiteObject;
                })
                .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean insertCollectedSite(JunZiCollectedSiteObject junZiCollectedSiteObject) {
        return super.save(junZiCollectedSiteConvert.transferToDo(junZiCollectedSiteObject));
    }
}
