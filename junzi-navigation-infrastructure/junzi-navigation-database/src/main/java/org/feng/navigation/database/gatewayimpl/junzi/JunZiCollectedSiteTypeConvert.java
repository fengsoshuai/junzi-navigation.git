package org.feng.navigation.database.gatewayimpl.junzi;

import org.feng.navigation.database.entity.JunZiCollectedSiteTypeDO;
import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteTypeObject;
import org.mapstruct.Mapper;

/**
 * TODO 类的描述
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时09分
 */
@Mapper(componentModel = "spring")
public interface JunZiCollectedSiteTypeConvert {
    JunZiCollectedSiteTypeObject transferToDomainObject(JunZiCollectedSiteTypeDO junZiCollectedSiteTypeDO);

    JunZiCollectedSiteTypeDO transferToDo(JunZiCollectedSiteTypeObject junZiCollectedSiteTypeObject);
}
