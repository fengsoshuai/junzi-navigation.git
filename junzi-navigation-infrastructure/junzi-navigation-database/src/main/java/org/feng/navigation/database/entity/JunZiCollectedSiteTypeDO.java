package org.feng.navigation.database.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 09时39分
 */
@Data
@TableName("collected_site_type")
public class JunZiCollectedSiteTypeDO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String type;
}
