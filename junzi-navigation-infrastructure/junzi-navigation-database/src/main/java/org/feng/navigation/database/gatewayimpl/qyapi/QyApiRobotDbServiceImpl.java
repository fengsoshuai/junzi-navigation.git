package org.feng.navigation.database.gatewayimpl.qyapi;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.common.util.StringUtil;
import org.feng.navigation.database.entity.QyApiRobotDO;
import org.feng.navigation.database.mapper.QyApiRobotMapper;
import org.feng.navigation.domain.qyapi.entity.QyApiRobotKeyObject;
import org.feng.navigation.domain.rpc.qyapi.gateway.QyApiRobotDbService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 企业微信机器人数据库服务实现
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 17时05分
 */
@Slf4j
@Service
public class QyApiRobotDbServiceImpl extends ServiceImpl<QyApiRobotMapper, QyApiRobotDO> implements QyApiRobotDbService {

    @Cacheable(cacheNames = StringUtil.ROBOT_KEY_CACHE_NAME, key = "#id", unless = "#result == null")
    @Override
    public QyApiRobotKeyObject findOneRobot(Integer id) {
        QyApiRobotDO apiRobotDO = super.getById(id);
        if (apiRobotDO == null) {
            return null;
        }
        QyApiRobotKeyObject qyApiRobotKeyObject = new QyApiRobotKeyObject();
        qyApiRobotKeyObject.setRobotKey(apiRobotDO.getRobotKey());
        return qyApiRobotKeyObject;
    }

    @Override
    public List<QyApiRobotKeyObject> findAll() {
        List<QyApiRobotDO> qyApiRobots = super.list();
        if (CollectionUtils.isEmpty(qyApiRobots)) {
            return Collections.emptyList();
        }

        return qyApiRobots.stream().map(robotDo -> {
            QyApiRobotKeyObject qyApiRobotKeyObject = new QyApiRobotKeyObject();
            qyApiRobotKeyObject.setRobotName(robotDo.getRobotName());
            qyApiRobotKeyObject.setId(robotDo.getId());
            return qyApiRobotKeyObject;
        }).collect(Collectors.toList());
    }
}
