package ${fullPackageName};

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * TODO
 *
 * @version ${version}
 * @author: ${author}
 * @date: ${date}
 */
@Getter
@AllArgsConstructor
public enum ${customEnumClassName} {

    <#list enumBodyObjects as enumBodyObject>
    /**
     * ${enumBodyObject.doc}
     */
    ${enumBodyObject.body},
    </#list>
    ;
    <#list fieldObjects as fieldObject>
    /**
     * ${fieldObject.fieldDoc}
     */
    private final ${fieldObject.simpleFieldType} ${fieldObject.fieldName};
    </#list>

    public static ${customEnumClassName} of(${keyObject.simpleFieldType} ${keyObject.fieldName}) {
        if (${keyObject.fieldName} == null) {
            throw new IllegalArgumentException("${keyObject.nonNullMessage}");
        }
        return Arrays.stream(values())
                .filter(element -> element.${keyObject.fieldName}.equals(${keyObject.fieldName}))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("${keyObject.nonExistMessage}"));
    }
}
