package org.feng.navigation.generator.gatewayimpl;

import freemarker.template.Template;
import org.feng.navigation.generator.util.FreemarkerUtil;
import org.springframework.stereotype.Component;

/**
 * 自定义枚举文件-自动生成客户端
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 16时50分
 */
@Component
public class CustomEnumAutoGeneratorClient extends BaseGeneratorClient {

    private static final String TEMPLATE_NAME = "GeneratorEnum.java.ftl";

    @Override
    protected Template getTemplate() {
        return FreemarkerUtil.getTemplateByName(TEMPLATE_NAME);
    }
}
