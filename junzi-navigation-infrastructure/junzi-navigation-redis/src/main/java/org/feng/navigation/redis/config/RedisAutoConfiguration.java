package org.feng.navigation.redis.config;

import org.feng.navigation.redis.bean.CustomRedisProperties;
import org.feng.navigation.redis.client.CustomRedisClient;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;

/**
 * Redis 自动配置
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月14日 10时26分
 */
@ComponentScan(basePackages = "org.feng.navigation.redis")
@AutoConfiguration
public class RedisAutoConfiguration {


    /**
     * 创建模板对象，使用自定义的序列化方式
     *
     * @param factory redis连接工厂
     * @param context 我们手动指定的序列化方式
     * @return RedisTemplate 实例
     */
    @Primary
    @Bean
    @ConditionalOnBean(ReactiveRedisConnectionFactory.class)
    public ReactiveRedisTemplate<String, Object> reactiveRedisTemplate(ReactiveRedisConnectionFactory factory, DefaultRedisSerializationContext context) {
        return new ReactiveRedisTemplate<>(factory, context);
    }

    @Primary
    @Bean(name = "customRedisProperties")
    @ConfigurationProperties(prefix = "junzi.redis")
    @ConditionalOnProperty(prefix = "junzi.redis", name = "enable")
    public CustomRedisProperties customRedisProperties() {
        return CustomRedisProperties.builder().build();
    }

    @Primary
    @Bean
    @ConditionalOnBean(name = {"customRedisProperties", "reactiveRedisTemplate"})
    public CustomRedisClient customRedisClient(ReactiveRedisTemplate<String, Object> reactiveRedisTemplate, CustomRedisProperties customRedisProperties) {
        return new CustomRedisClient(reactiveRedisTemplate, customRedisProperties);
    }
}
