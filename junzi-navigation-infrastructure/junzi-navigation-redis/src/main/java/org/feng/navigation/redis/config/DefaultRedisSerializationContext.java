package org.feng.navigation.redis.config;

import lombok.NonNull;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;


/**
 * Redis 序列化的上下文
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2022年11月06日 09时54分
 */
@Component
public class DefaultRedisSerializationContext implements RedisSerializationContext<String, Object> {
    private final StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
    private final GenericJackson2JsonRedisSerializer genericJackson2JsonRedisSerializer = new GenericJackson2JsonRedisSerializer();

    /**
     * 设置key的序列化方式
     */
    @Override
    public @NonNull SerializationPair<String> getKeySerializationPair() {
        return SerializationPair.fromSerializer(stringRedisSerializer);
    }

    /**
     * 设置value的序列化方式
     */
    @Override
    public @NonNull SerializationPair<Object> getValueSerializationPair() {
        return SerializationPair.fromSerializer(genericJackson2JsonRedisSerializer);
    }

    /**
     * 设置hashkey的序列化方式
     */
    @SuppressWarnings("unchecked")
    @Override
    public @NonNull SerializationPair<String> getHashKeySerializationPair() {
        return SerializationPair.fromSerializer(stringRedisSerializer);
    }

    /**
     * 设置hashvalue的序列化方式
     */
    @SuppressWarnings("unchecked")
    @Override
    public @NonNull SerializationPair<Object> getHashValueSerializationPair() {
        return SerializationPair.fromSerializer(genericJackson2JsonRedisSerializer);
    }

    /**
     * 设置spring字符串的序列化方式
     */
    @Override
    public @NonNull SerializationPair<String> getStringSerializationPair() {
        return SerializationPair.fromSerializer(stringRedisSerializer);
    }
}