package org.feng.navigation.redis.bean;

import lombok.Data;

import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * 自定义redis配置字段实体
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年11月06日 11时45分
 */
@Data
public class CustomRedisBean {
    /**
     * 业务前缀
     */
    private String businessPrefix;
    /**
     * 缓存失效时间
     */
    private Integer businessExpireTime;
    /**
     * 失效时间单位
     */
    private ChronoUnit expireTimeUnit;
    /**
     * 是否启用配置
     */
    private Boolean enable;

    /**
     * 校验参数
     * @return 校验通过返回true；否则返回false
     */
    public boolean validate() {
        if (Boolean.TRUE.equals(enable)) {
            Objects.requireNonNull(businessExpireTime, "缓存失效时间");
            Objects.requireNonNull(expireTimeUnit, "缓存失效时间单位");
            Objects.requireNonNull(businessPrefix, "业务前缀");
            return true;
        }
        return false;
    }
}
