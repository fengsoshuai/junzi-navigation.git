package org.feng.navigation.redis.bean;

import lombok.Builder;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * 自定义redis业务属性
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年11月06日 10时11分
 */
@Slf4j
@Builder
public class CustomRedisProperties {
    /**
     * 定义多个业务配置：不对外暴露<br>
     * key为业务别名，value为对应业务的配置信息
     */
    @Setter
    private Map<String, CustomRedisBean> business;

    @PostConstruct
    private void init() {
        log.info("注册自定义redis业务配置：{}", business);
    }

    public CustomRedisBean getCustomRedisBeanByKey(String businessKey) {
        return business.get(businessKey);
    }
}
