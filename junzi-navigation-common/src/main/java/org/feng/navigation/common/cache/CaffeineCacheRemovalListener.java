package org.feng.navigation.common.cache;

import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.RemovalListener;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * 监听删除缓存操作
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月13日 16时08分
 */
@Slf4j
public class CaffeineCacheRemovalListener implements RemovalListener<Object, Object> {
    @Override
    public void onRemoval(@Nullable Object key, @Nullable Object value, @NonNull RemovalCause cause) {
        log.info("正在移除缓存【{}】【{}】-【{}】", key, value, cause);
    }
}
