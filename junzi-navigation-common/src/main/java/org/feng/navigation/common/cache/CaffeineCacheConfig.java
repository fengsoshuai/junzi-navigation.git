package org.feng.navigation.common.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalListener;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.ForkJoinPool;

/**
 * <code>Caffeine</code> 缓存配置
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月13日 17时37分
 */
@Configuration
public class CaffeineCacheConfig {
    @Primary
    @Bean("cacheManager")
    public CaffeineCacheManager cacheManager() {
        ForkJoinPool pool = new ForkJoinPool(5);
        CaffeineCacheManager cacheManager = new CaffeineCacheManager();
        Caffeine<Object, Object> caffeine = Caffeine.newBuilder()
                .executor(pool)
                .removalListener(removalListener())
                .maximumSize(1000);
        cacheManager.setCaffeine(caffeine);
        return cacheManager;
    }

    @Bean
    public RemovalListener<Object, Object> removalListener() {
        return new CaffeineCacheRemovalListener();
    }
}
