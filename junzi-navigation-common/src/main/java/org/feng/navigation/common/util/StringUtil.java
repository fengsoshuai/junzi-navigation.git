package org.feng.navigation.common.util;

import cn.hutool.core.util.StrUtil;

import java.util.Locale;
import java.util.UUID;

/**
 * 字符串工具类
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月12日 10时50分
 */
public class StringUtil {

    private static final String EMPTY_STR = "";

    /**
     * robot 的key对应的缓存名
     */
    public static final String ROBOT_KEY_CACHE_NAME = "robot_key_cache";

    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase(Locale.ROOT);
    }



    /**
     * 字符串截取，将一个字符串去除两端空格后，截取前 length位字符<br>
     * 如果length为负数，则返回原来的字符串<br>
     * 如果原来的字符串包含了空格，则会去除两端空格，再进行截取<br>
     * <b>示例代码</b>：
     * <ul>
     *     <li>{@code substringWithStrip(" abcdefghijk ", 3)} => {@code "abc"}</li>
     *     <li>{@code substringWithStrip(" abcdefghijk ", 100)} => {@code "abcdefghijk"}</li>
     *     <li>{@code substringWithStrip("abcdefghijk ", -1)} => {@code " abcdefghijk "}</li>
     *     <li>{@code substringWithStrip("abcdefghijk ", 0)} => {@code ""}</li>
     *     <li>{@code substringWithStrip("abcdefghijk ", 3)} => {@code "abc"}</li>
     *     <li>{@code substringWithStrip("", 4)} => {@code ""}</li>
     *     <li>{@code substringWithStrip(null, 5)} => {@code ""}</li>
     *     <li>{@code substringWithStrip("  ", 3)} => {@code ""}</li>
     * </ul>
     *
     * @param string 要截取的字符串
     * @param length 要截取的长度
     * @return 截取后的字符串；当要截取的字符串为 <code>null, "", "null", "{}", "[]", "()"</code> 之一或全部字符为空格时，返回空字符串；
     * 当截取长度大于字符串去除两端空格后长度时，返回去除两端空格后的字符串；当截取长度小于0时，抛出非法参数异常
     */
    public static String substringWithStrip(String string, int length) {
        if (length < 0) {
            throw new IllegalArgumentException("截取字符串时参数异常，截取长度不能为 " + length);
        }
        if (StrUtil.isEmpty(string)) {
            return EMPTY_STR;
        }

        // 去除两端空格
        string = string.strip();
        int currentLength = string.length();
        if (currentLength < length) {
            return string;
        }

        return string.substring(0, length);
    }

    /**
     * 字符串截取，将一个字符串截取前 length 位字符<br>
     * 若考虑空格，请参看 {@link StringUtil#substringWithStrip(String, int)} <br>
     * <b>代码示例：</b>
     * <ul>
     *     <li>{@code substring(" abcdefghijk ", -3)} => {@code " abcdefghijk "}</li>
     *     <li>{@code substring(" abcdefghijk ", 0)} => {@code ""}</li>
     *     <li>{@code substring(" abcdefghijk ", 4)} => {@code " abc"}</li>
     *     <li>{@code substring(" abcdefghijk ", 88)} => {@code " abcdefghijk "}</li>
     *     <li>{@code substring("abcdefghijk ", 4)} => {@code "abcd"}</li>
     * </ul>
     *
     * @param string 要截取的字符串
     * @param length 要截取的长度
     * @return 截取后的字符串；当要截取的字符串为null或空字符串时，返回空字符串；当截取长度大于字符串长度本身时，返回字符串本身；
     * 当截取长度小于0时，抛出非法参数异常
     */
    public static String substring(String string, int length) {
        if (length < 0) {
            throw new IllegalArgumentException("截取字符串时参数异常，截取长度不能为 " + length);
        }
        if (EMPTY_STR.equals(string) || string == null) {
            return EMPTY_STR;
        }

        // 获取字符串长度
        int currentLength = string.length();
        if (currentLength < length) {
            return string;
        }
        return string.substring(0, length);
    }
}
