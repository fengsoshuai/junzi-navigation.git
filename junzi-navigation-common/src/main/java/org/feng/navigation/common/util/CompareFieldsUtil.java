package org.feng.navigation.common.util;

import cn.hutool.core.collection.CollectionUtil;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 任意两个同类型对象属性比较：可以比较全部属性或指定属性进行比较
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月08日 14时51分
 */
@Slf4j
public class CompareFieldsUtil {

    public static final String SRC_VALUE_KEY = "srcValue";
    public static final String NEW_VALUE_KEY = "newValue";

    /**
     * 同类型两个对象的单个属性
     *
     * @param srcData      对象1
     * @param newData      对象2
     * @param propertyName 属性名
     * @return true 表示当前属性值相同；false 表示不同
     */
    public static boolean compareSingleField(@NonNull Object srcData, @NonNull Object newData, @NonNull String propertyName) {
        if (!srcData.getClass().equals(newData.getClass())) {
            return false;
        }
        Map<String, Map<String, Object>> differInfoMap = new HashMap<>(16);
        return compareSingleField(differInfoMap, srcData, newData, propertyName);
    }

    /**
     * 同类型两个对象的单个属性
     *
     * @param srcData       对象1
     * @param newData       对象2
     * @param propertyName  属性名
     * @param differInfoMap 属性信息不同时，存储到map里。对应的键值关系：key是属性名称，对应的value是另一个Map，该Map的键值是存的两个对象对应该属性的值
     * @return true 表示当前属性值相同；false 表示不同
     */
    public static boolean compareSingleField(@NonNull Map<String, Map<String, Object>> differInfoMap, @NonNull Object srcData, @NonNull Object newData, @NonNull String propertyName) {
        if (!srcData.getClass().equals(newData.getClass())) {
            return false;
        }
        try {
            PropertyDescriptor propertyDescriptor = BeanUtils.getPropertyDescriptor(srcData.getClass(), propertyName);
            if (Objects.nonNull(propertyDescriptor)) {
                // 获取属性的get方法
                Method readMethod = propertyDescriptor.getReadMethod();
                if (Objects.nonNull(readMethod)) {
                    // 获取属性值
                    Object srcValue = readMethod.invoke(srcData);
                    Object newValue = readMethod.invoke(newData);
                    // 比较属性值
                    boolean valueIsEquals = Objects.equals(srcValue, newValue);
                    if (!valueIsEquals) {
                        HashMap<String, Object> valueMapper = new HashMap<>(4);
                        valueMapper.put(SRC_VALUE_KEY, srcValue);
                        valueMapper.put(NEW_VALUE_KEY, newValue);
                        differInfoMap.put(propertyName, valueMapper);
                        return false;
                    }
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.info("比较属性出错", e);
            return false;
        }
        return true;
    }

    /**
     * 同类型两个对象的所有属性
     *
     * @param srcData 对象1
     * @param newData 对象2
     * @return true 表示当前属性值相同；false 表示不同
     */
    public static boolean compareFields(@NonNull Object srcData, @NonNull Object newData) {
        if (!srcData.getClass().equals(newData.getClass())) {
            return false;
        }
        Map<String, Map<String, Object>> differInfoMap = new HashMap<>(16);
        return compareFields(differInfoMap, srcData, newData);
    }

    /**
     * 同类型两个对象的所有属性
     *
     * @param srcData       对象1
     * @param newData       对象2
     * @param differInfoMap 属性信息不同时，存储到map里。对应的键值关系：key是属性名称，value存储了不同的值
     * @return true 表示当前属性值相同；false 表示不同
     */
    public static boolean compareFields(@NonNull Map<String, Map<String, Object>> differInfoMap, @NonNull Object srcData, @NonNull Object newData) {
        if (!srcData.getClass().equals(newData.getClass())) {
            return false;
        }
        try {
            PropertyDescriptor[] propertyDescriptor = BeanUtils.getPropertyDescriptors(srcData.getClass());
            for (PropertyDescriptor descriptor : propertyDescriptor) {
                // 获取属性对应的get方法
                Method readMethod = descriptor.getReadMethod();
                // 属性名
                String name = descriptor.getName();
                if (Objects.nonNull(readMethod)) {
                    // 获取属性值
                    Object srcValue = readMethod.invoke(srcData);
                    Object newValue = readMethod.invoke(newData);
                    // 比较属性值
                    boolean valueIsEquals = Objects.equals(srcValue, newValue);
                    if (!valueIsEquals) {
                        HashMap<String, Object> valueMapper = new HashMap<>(4);
                        valueMapper.put(SRC_VALUE_KEY, srcValue);
                        valueMapper.put(NEW_VALUE_KEY, newValue);
                        differInfoMap.put(name, valueMapper);
                    }
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.info("比较属性出错", e);
            return false;
        }
        return CollectionUtil.isEmpty(differInfoMap);
    }

    // public static void main(String[] args) {
    //     MerchantDTO srcData = new MerchantDTO();
    //     srcData.setProduct("er212");
    //     srcData.setDisabled(1);
    //
    //     MerchantDTO newData = new MerchantDTO();
    //     newData.setProduct("er7212");
    //     newData.setDisabled(null);
    //
    //     Map<String, Map<String, Object>> differInfoMap = new HashMap<>(16);
    //     boolean compare = compareFields(differInfoMap, srcData, newData);
    //     // {product={newValue=er7212, srcValue=er212}, disabled={newValue=null, srcValue=1}}
    //     System.out.println(differInfoMap);
    //     // false
    //     System.out.println(compare);
    // }
    //
    // @Data
    // public static class MerchantDTO {
    //     private String product;
    //     private String merchantId;
    //     private Integer disabled;
    // }
}
