package org.feng.navigation.common.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 时间工具类
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 16时08分
 */
public class TimeUtil {
    private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern(TimeFormatPatternEnum.DATETIME_FORMAT_PATTERN.toString());
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(TimeFormatPatternEnum.DATE_FORMAT_PATTERN.toString());
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern(TimeFormatPatternEnum.DATE_FORMAT.toString());

    /**
     * 将年月日（yyyy-MM-dd）的字符串转换为实例
     *
     * @param timeString 时间字符串
     * @return LocalDate实例
     */
    public static LocalDate parse(String timeString) {
        return LocalDate.parse(timeString, DATE_FORMATTER);
    }

    /**
     * 将日期时间转换为 （yyyy年MM月dd日 HH时mm分）
     *
     * @param localDateTime 日期时间
     * @return 指定格式的字符串
     */
    public static String format(LocalDateTime localDateTime) {
        return localDateTime.format(DATE_FORMAT);
    }

    /**
     * 将当前日期时间转换为 （yyyy年MM月dd日 HH时mm分）
     *
     * @return 指定格式的字符串
     */
    public static String formatCurrent() {
        return LocalDateTime.now().format(DATE_FORMAT);
    }
    
    /**
     * 将当前日期时间转换为 （yyyy-MM-dd）
     *
     * @return 指定格式的字符串
     */
    public static String formatCurrentDate() {
        return LocalDateTime.now().format(DATE_FORMATTER);
    }


    /**
     * 时间格式枚举
     */
    public enum TimeFormatPatternEnum {
        /**
         * 时间格式枚举：年月日，年月日时分秒，年月日时分秒时区
         */
        DATE_FORMAT_PATTERN("yyyy-MM-dd"),
        DATETIME_FORMAT_PATTERN("yyyy-MM-dd HH:mm:ss"),
        DATETIME_ZONE_FORMAT_PATTERN("yyyy-MM-dd HH:mm:ssZ"),
        DATE_FORMAT("yyyy年MM月dd日 HH时mm分");

        private final String format;

        TimeFormatPatternEnum(String format) {
            this.format = format;
        }

        @Override
        public String toString() {
            return format;
        }
    }
}
