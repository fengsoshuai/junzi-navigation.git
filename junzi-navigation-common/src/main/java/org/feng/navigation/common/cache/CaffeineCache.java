package org.feng.navigation.common.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Caffeine 缓存
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月13日 18时14分
 */
@Slf4j
@Component
public class CaffeineCache {
    @Resource
    private CaffeineCacheManager cacheManager;

    /**
     * 获取缓存
     *
     * @param cacheName 缓存名
     * @param key       缓存键
     * @return 查到的缓存值
     */
    public Object getCache(String cacheName, Object key) {
        log.debug("获取缓存【{}】【{}】", cacheName, key);
        Cache cache = cacheManager.getCache(cacheName);
        if (cache == null) {
            return null;
        }
        Cache.ValueWrapper valueWrapper = cache.get(key);
        if (valueWrapper == null) {
            return null;
        }
        return valueWrapper.get();
    }

    /**
     * 存储缓存
     *
     * @param cacheName 缓存名
     * @param key       缓存键
     * @param value     要放入缓存的缓存值
     */
    public void putCache(String cacheName, Object key, Object value) {
        log.debug("存入缓存【{}】【{}】-【{}】", cacheName, key, value);
        Cache cache = cacheManager.getCache(cacheName);
        if (cache != null) {
            cache.put(key, value);
        }
    }

    /**
     * 清理缓存
     *
     * @param cacheName 缓存名
     * @param key       缓存键
     */
    public void evictCache(String cacheName, Object key) {
        Cache cache = cacheManager.getCache(cacheName);
        if (cache != null) {
            cache.evict(key);
        }
    }
}
