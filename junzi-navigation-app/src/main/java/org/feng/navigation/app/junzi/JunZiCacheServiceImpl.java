package org.feng.navigation.app.junzi;

import org.feng.navigation.client.api.JunZiCacheService;
import org.feng.navigation.client.dto.junzi.JunZiClearCacheRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiClearCacheResponseDTO;
import org.feng.navigation.common.cache.CaffeineCache;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 菌子缓存服务实现
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月12日 15时16分
 */
@Service
public class JunZiCacheServiceImpl implements JunZiCacheService {

    @Resource
    private CaffeineCache caffeineCache;

    @Override
    public JunZiClearCacheResponseDTO clearCache(JunZiClearCacheRequestDTO requestDTO) {
        JunZiClearCacheResponseDTO responseDTO = new JunZiClearCacheResponseDTO();
        caffeineCache.evictCache(requestDTO.getCacheName(), requestDTO.getKey());
        responseDTO.setSuccess(true);
        return responseDTO;
    }
}
