package org.feng.navigation.app.junzi;

import org.feng.navigation.client.api.JunZiManagerService;
import org.feng.navigation.client.dto.junzi.JunZiManagerDTO;
import org.feng.navigation.database.gatewayimpl.junzi.JunZiManagerConvert;
import org.feng.navigation.domain.junzi.domainobject.JunZiManagerObject;
import org.feng.navigation.domain.junzi.gateway.JunZiManagerDbService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月19日 12时34分
 */
@Service
public class JunZiManagerServiceImpl implements JunZiManagerService {

    @Resource
    private JunZiManagerDbService junZiManagerDbService;
    @Resource
    private JunZiManagerConvert junZiManagerConvert;

    @Override
    public List<JunZiManagerDTO> listJunZiManager() {

        List<JunZiManagerObject> junZiManagerObjects = junZiManagerDbService.listJunZiManager();

        return junZiManagerConvert.transferToDtoList(junZiManagerObjects);
    }
}
