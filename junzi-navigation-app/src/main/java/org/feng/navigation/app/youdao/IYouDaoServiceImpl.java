package org.feng.navigation.app.youdao;

import org.feng.navigation.client.api.IYouDaoService;
import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateRequestDTO;
import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateResponseDTO;
import org.feng.navigation.domain.rpc.youdao.domainservice.YouDaoApiService;
import org.feng.navigation.domain.youdao.entity.TextTranslateObject;
import org.feng.navigation.rest.gatewayimpl.youdao.YouDaoTextTranslateConvert;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 有道-业务服务实现
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月10日 10时42分
 */
@Service
public class IYouDaoServiceImpl implements IYouDaoService {
    @Resource
    private YouDaoApiService youDaoApiService;
    @Resource
    private YouDaoTextTranslateConvert youDaoTextTranslateConvert;

    @Override
    public YouDaoTextTranslateResponseDTO textTranslate(YouDaoTextTranslateRequestDTO youDaoTextTranslateRequestDTO) {
        TextTranslateObject textTranslateObject = youDaoTextTranslateConvert.transferToDomainObject(youDaoTextTranslateRequestDTO);
        youDaoApiService.textTranslate(textTranslateObject);
        return new YouDaoTextTranslateResponseDTO().setTranslation(textTranslateObject.getTranslation());
    }
}
