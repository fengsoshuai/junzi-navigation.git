package org.feng.navigation.app.generator;

import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.client.api.GeneratorService;
import org.feng.navigation.client.dto.generator.GeneratorCustomEnumRequestDTO;
import org.feng.navigation.client.dto.generator.GeneratorCustomEnumResponseDTO;
import org.feng.navigation.domain.generator.domainobject.EnumBodyObject;
import org.feng.navigation.domain.generator.domainobject.EnumGeneratorObject;
import org.feng.navigation.domain.generator.domainservice.GeneratorApiService;
import org.feng.navigation.generator.definition.EnumFileContentDefinition;
import org.feng.navigation.generator.util.FileUtil;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Stream;

/**
 * 生成器服务
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 20时25分
 */
@Slf4j
@Service
public class GeneratorServiceImpl implements GeneratorService {

    @Resource
    private GeneratorApiService generatorApiService;

    @Resource
    private ThreadPoolTaskExecutor asyncExecutor;

    @Override
    public GeneratorCustomEnumResponseDTO generatorCustomEnum(GeneratorCustomEnumRequestDTO requestDTO) {
        EnumGeneratorObject enumGeneratorObject = new EnumGeneratorObject();
        String text = requestDTO.getText();
        int ketIndex = requestDTO.getKeyIndex();

        List<CompletableFuture<Void>> futures = new ArrayList<>();
        List<EnumBodyObject> enumBodys = new CopyOnWriteArrayList<>();
        // 按照换行符切割
        Stream.of(text.split("\r\n")).forEach(line -> {
            GenerateEnumRunnable generateEnumRunnable = new GenerateEnumRunnable();
            generateEnumRunnable.enumBodys = enumBodys;
            generateEnumRunnable.ketIndex = ketIndex;
            generateEnumRunnable.line = line;
            futures.add(CompletableFuture.runAsync(generateEnumRunnable, asyncExecutor));
        });
        // 等待所有的异步请求任务执行完成
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();

        enumGeneratorObject.setEnumBodyList(enumBodys);
        // 设置枚举类名
        enumGeneratorObject.setCustomEnumClassName(requestDTO.getCustomEnumClassName());
        // 设置枚举类所在的完整包名
        enumGeneratorObject.setFullPackageName(requestDTO.getFullPackageName());
        // 生成完整枚举内容
        generatorApiService.generateCustomEnum(enumGeneratorObject);
        String generatedContent = enumGeneratorObject.getGeneratedContent();
        GeneratorCustomEnumResponseDTO responseDTO = new GeneratorCustomEnumResponseDTO();
        responseDTO.setGeneratedContent(generatedContent);
        /// 生成文件内容写到磁盘
        // FileUtil.writeFileToDisk(generatedContent, "generatedDir", requestDTO.getCustomEnumClassName() + ".java");
        return responseDTO;
    }

    private static class GenerateEnumRunnable implements Runnable {
        private String line;
        private Integer ketIndex;
        private List<EnumBodyObject> enumBodys;

        @Override
        public void run() {
            // 按逗号切割
            String[] splitBySpace = line.split(",");
            List<String> contents = new ArrayList<>();
            for (int i = 0; i < splitBySpace.length; i++) {
                if (ketIndex != i) {
                    contents.add(splitBySpace[i]);
                }
            }
            EnumFileContentDefinition enumFileContentDefinition = new EnumFileContentDefinition(splitBySpace[ketIndex], contents);
            // 组装枚举名、参数、注释
            EnumBodyObject bodyObject = new EnumBodyObject();
            String pinYinEnumName = enumFileContentDefinition.getPinYinEnumName();
            bodyObject.setDoc(enumFileContentDefinition.getOriginChineseBody() + "：" + enumFileContentDefinition.getKey());
            bodyObject.setBody(pinYinEnumName);
            log.debug("线程{}异步组装枚举中...", Thread.currentThread().getName());
            enumBodys.add(bodyObject);
        }
    }
}
