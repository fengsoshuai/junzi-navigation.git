package org.feng.navigation.app.junzi;

import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.client.api.JunZiCollectedSiteService;
import org.feng.navigation.client.dto.junzi.JunZiAddCollectedSiteRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiAddCollectedSiteResponseDTO;
import org.feng.navigation.client.dto.junzi.JunZiListCollectedSiteRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiListCollectedSiteResponseDTO;
import org.feng.navigation.database.gatewayimpl.junzi.JunZiCollectedSiteConvert;
import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteObject;
import org.feng.navigation.domain.junzi.domainservice.JunZiCollectedSiteApiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * TODO 类的描述
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时45分
 */
@Slf4j
@Service
public class JunZiCollectedSiteServiceImpl implements JunZiCollectedSiteService {
    @Resource
    private JunZiCollectedSiteApiService junZiCollectedSiteApiService;
    @Resource
    private JunZiCollectedSiteConvert junZiCollectedSiteConvert;

    @Override
    public JunZiAddCollectedSiteResponseDTO addCollectedSite(JunZiAddCollectedSiteRequestDTO requestDTO) {
        JunZiCollectedSiteObject junZiCollectedSiteObject = junZiCollectedSiteConvert.addDtoToDomainObject(requestDTO);
        boolean addSuccess = junZiCollectedSiteApiService.addJunZiCollectedSite(junZiCollectedSiteObject);
        JunZiAddCollectedSiteResponseDTO responseDTO = new JunZiAddCollectedSiteResponseDTO();
        responseDTO.setSuccess(addSuccess);
        return responseDTO;
    }

    @Override
    public JunZiListCollectedSiteResponseDTO listCollectedSites(JunZiListCollectedSiteRequestDTO requestDTO) {
        Integer currentPage = requestDTO.getCurrentPage();
        Integer pageSize = requestDTO.getPageSize();
        JunZiListCollectedSiteResponseDTO responseDTO = new JunZiListCollectedSiteResponseDTO();

        List<JunZiCollectedSiteObject> junZiCollectedSiteObjects = junZiCollectedSiteApiService.listJunZiCollectedSites(currentPage, pageSize);
        responseDTO.setCollectedSiteDataBodyList(junZiCollectedSiteConvert.transferToListDto(junZiCollectedSiteObjects));
        return responseDTO;
    }
}
