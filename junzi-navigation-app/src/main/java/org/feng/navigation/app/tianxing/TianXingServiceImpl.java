package org.feng.navigation.app.tianxing;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.client.api.TianXingService;
import org.feng.navigation.client.dto.tianxing.*;
import org.feng.navigation.domain.rpc.tianxing.domainservice.TianXingApiService;
import org.feng.navigation.domain.tianxing.entity.*;
import org.feng.navigation.domain.tianxing.enums.TianXingWeatherSearchTypeEnum;
import org.feng.navigation.rest.gatewayimpl.tianxing.TianXingConvert;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 天行数据业务服务实现
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 16时29分
 */
@Slf4j
@Service
public class TianXingServiceImpl implements TianXingService {
    @Resource
    private TianXingApiService tianXingApiService;
    @Resource
    private TianXingConvert tianXingConvert;

    @Override
    public TianXingAllNewsResponseDTO allNews(TianXingAllNewsRequestDTO requestDTO) {
        TianXingAllNewsObject tianXingAllNewsObject = tianXingConvert.transferAllNewsDomainObject(requestDTO);
        tianXingApiService.tianXingAllNews(tianXingAllNewsObject);

        TianXingAllNewsResponseDTO responseDTO = new TianXingAllNewsResponseDTO();
        // 发生错误
        if (StrUtil.isNotEmpty(tianXingAllNewsObject.getErrorMessage())) {
            responseDTO.setErrorMsg(tianXingAllNewsObject.getErrorMessage());
            return responseDTO;
        }
        // 处理成功信息
        List<TianXingAllNewsResponseDTO.TianXingAllNewsData> resultNewsList = tianXingAllNewsObject.getNewsList().stream().map(data -> {
            TianXingAllNewsResponseDTO.TianXingAllNewsData newsData = new TianXingAllNewsResponseDTO.TianXingAllNewsData();
            newsData.setDate(data.getDate());
            newsData.setUrl(data.getUrl());
            newsData.setTitle(data.getTitle());
            newsData.setSource(data.getSource());
            newsData.setPicUrl(data.getPicUrl());
            return newsData;
        }).collect(Collectors.toList());
        responseDTO.setNewsList(resultNewsList);
        return responseDTO;
    }

    @Override
    public TianXingWeatherSearchResponseDTO weatherSearch(TianXingWeatherSearchRequestDTO requestDTO) {
        TianXingWeatherSearchResponseDTO responseDTO = new TianXingWeatherSearchResponseDTO();
        TianXingAreaSearchObject areaSearchObject = new TianXingAreaSearchObject();
        areaSearchObject.setProvince(requestDTO.getProvince());
        areaSearchObject.setArea(requestDTO.getArea());
        tianXingApiService.tianXingAreaSearch(areaSearchObject);
        // 没查到区域天气ID、发生错误
        if (areaSearchObject.hasError()) {
            responseDTO.setErrorMsg(areaSearchObject.getErrorMessage());
            return responseDTO;
        }

        TianXingWeatherSearchObject weatherSearchObject = new TianXingWeatherSearchObject();
        weatherSearchObject.setCity(areaSearchObject.getAreaId());
        weatherSearchObject.setTypeEnum(TianXingWeatherSearchTypeEnum.REAL_TIME_WEATHER);
        tianXingApiService.tianXingWeatherSearch(weatherSearchObject);
        // 没查到天气数据、发生错误
        if (weatherSearchObject.hasError()) {
            responseDTO.setErrorMsg(areaSearchObject.getErrorMessage());
            return responseDTO;
        }
        // 天气数据
        TianXingWeatherSearchResultData resultData = weatherSearchObject.getResultData();
        return tianXingConvert.transferToTianXingWeatherSearchResponseDTO(resultData);
    }

    @Override
    public TianXingRobotChatResponseDTO chatRobot(TianXingRobotChatRequestDTO requestDTO) {
        TianXingRobotChatResponseDTO responseDTO = new TianXingRobotChatResponseDTO();
        TianXingRobotObject robotObject = new TianXingRobotObject();
        robotObject.setUniqueId(requestDTO.getUniqueId());
        robotObject.setQuestion(requestDTO.getQuestion());
        tianXingApiService.tianXingChatRobot(robotObject);
        // 发生错误
        if (robotObject.hasError()) {
            responseDTO.setErrorMsg(robotObject.getErrorMessage());
            return responseDTO;
        }
        responseDTO.setReply(robotObject.getReply());
        return responseDTO;
    }
}
