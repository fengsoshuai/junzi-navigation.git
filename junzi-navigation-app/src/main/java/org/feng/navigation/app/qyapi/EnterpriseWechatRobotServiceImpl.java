package org.feng.navigation.app.qyapi;

import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.client.api.EnterpriseWechatRobotService;
import org.feng.navigation.client.dto.qyapi.EnterpriseWechatRobotRequestDTO;
import org.feng.navigation.client.dto.qyapi.EnterpriseWechatRobotResponseDTO;
import org.feng.navigation.domain.qyapi.entity.EnterpriseWechatRobotObject;
import org.feng.navigation.domain.qyapi.enums.MessageType;
import org.feng.navigation.domain.rpc.qyapi.domainservice.EnterpriseWechatRobotApiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 企业微信机器人服务实现
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 16时17分
 */
@Slf4j
@Service
public class EnterpriseWechatRobotServiceImpl implements EnterpriseWechatRobotService {

    @Resource
    private EnterpriseWechatRobotApiService enterpriseWechatRobotApiService;

    @Override
    public EnterpriseWechatRobotResponseDTO robotSend(EnterpriseWechatRobotRequestDTO requestDTO) {
        EnterpriseWechatRobotObject enterpriseWechatRobotObject = new EnterpriseWechatRobotObject(MessageType.of(requestDTO.getMessageType()))
                .setContent(requestDTO.getContent())
                .setKey(requestDTO.getRobotKey());
        EnterpriseWechatRobotResponseDTO responseDTO = new EnterpriseWechatRobotResponseDTO();
        try {
            enterpriseWechatRobotApiService.send(enterpriseWechatRobotObject);
            responseDTO.setSuccess(Boolean.TRUE);
        } catch (IllegalArgumentException e) {
            responseDTO.setSuccess(Boolean.FALSE);
            responseDTO.setErrorMessage(e.getMessage());
            log.error("企业微信机器人发送消息失败：{}", e.getMessage());
        } catch (Exception e) {
            responseDTO.setSuccess(Boolean.FALSE);
        }
        return responseDTO;
    }
}
