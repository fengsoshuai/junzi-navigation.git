/*
 Navicat Premium Data Transfer
 Date: 14/03/2023 22:24:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for collected_site
-- ----------------------------
DROP TABLE IF EXISTS `collected_site`;
CREATE TABLE `collected_site`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站名称',
  `site_type` tinyint(0) NOT NULL COMMENT '网站类型，具体类型见 collected_site_type',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网址',
  `ico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网站ico',
  `logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网站logo',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网站具体描述信息',
  `created_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '本条数据创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '上一次修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_name_type`(`site_type`, `name`) USING BTREE,
  INDEX `nk_site_type`(`site_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collected_site
-- ----------------------------
INSERT INTO `collected_site` VALUES (1, 'B站', 2, 'https://www.bilibili.com/', 'https://www.bilibili.com/favicon.ico', NULL, 'B站是多元化的强大视频网站', '2023-02-15 20:03:24', '2023-02-15 20:03:19');
INSERT INTO `collected_site` VALUES (2, '博客园', 1, 'https://www.cnblogs.com/', 'https://www.cnblogs.com/favicon.ico', NULL, '开发者的网上家园', '2023-02-16 14:13:09', '2023-02-16 14:13:05');
INSERT INTO `collected_site` VALUES (3, 'CSDN', 1, 'https://www.csdn.net/', 'https://www.csdn.net/favicon.ico', NULL, '中文最大的技术社区', '2023-02-16 14:15:22', '2023-02-16 17:29:21');
INSERT INTO `collected_site` VALUES (4, '开源中国', 1, 'https://www.oschina.net/', 'https://www.oschina.net/favicon.ico', NULL, '目前国内最大的开源技术社区', '2023-02-16 17:29:15', '2023-02-16 17:29:14');
INSERT INTO `collected_site` VALUES (5, '掘金', 1, 'https://juejin.cn/', 'https://juejin.cn/favicon.ico', NULL, '一个帮助开发者成长的社区', '2023-02-16 19:45:25', '2023-02-16 19:44:33');
INSERT INTO `collected_site` VALUES (6, '51CTO', 1, 'https://www.51cto.com/', 'https://www.51cto.com/favicon.ico', NULL, '中国领先的技术网站', '2023-02-16 19:46:34', '2023-02-16 19:46:33');
INSERT INTO `collected_site` VALUES (7, '开发者头条', 1, 'https://toutiao.io/', 'https://toutiao.io/favicon.ico', '', '程序员分享平台', '2023-02-16 22:36:06', '2023-02-16 22:36:06');
INSERT INTO `collected_site` VALUES (8, 'JSON在线解析', 5, 'https://www.json.cn/', 'https://www.json.cn/favicon.ico', '', 'JSON在线解析与格式化处理', '2023-02-16 22:38:12', '2023-02-16 22:38:12');
INSERT INTO `collected_site` VALUES (9, 'Maven仓库', 5, 'https://mvnrepository.com/', 'https://mvnrepository.com/favicon.ico', '', 'Maven仓库', '2023-02-16 22:40:48', '2023-02-16 22:40:48');
INSERT INTO `collected_site` VALUES (10, '在线加密解密', 5, 'https://tool.oschina.net/encrypt', 'https://tool.oschina.net/favicon.ico', '', '在线加密解密', '2023-02-16 22:42:14', '2023-02-16 22:42:14');
INSERT INTO `collected_site` VALUES (11, 'Deepl翻译', 5, 'https://www.deepl.com/translator', 'https://www.deepl.com/favicon.ico', '', '在线翻译', '2023-02-16 22:43:36', '2023-02-16 22:43:36');
INSERT INTO `collected_site` VALUES (12, '在线绘图', 5, 'https://www.processon.com/', 'https://www.processon.com/favicon.ico', '', '免费在线思维导图流程图', '2023-02-16 22:45:07', '2023-02-16 22:45:07');
INSERT INTO `collected_site` VALUES (13, 'Spring 中文文档', 4, 'https://springdoc.cn/', 'https://springdoc.cn/favicon.ico', NULL, '提供最新的Spring Boot, Spring Cloud, Spring Security等Spring框架的官方中文文档', '2023-02-20 19:22:06', '2023-02-20 19:22:06');
INSERT INTO `collected_site` VALUES (14, '知乎', 4, 'https://www.zhihu.com/', 'https://www.zhihu.com/favicon.ico', NULL, '中文互联网高质量的问答社区和创作者聚集的原创内容平台', '2023-02-20 19:24:15', '2023-02-20 19:24:15');
INSERT INTO `collected_site` VALUES (15, 'BEJSON', 5, 'https://www.bejson.com/', 'https://www.bejson.com/favicon.ico', NULL, '在线JSON校验格式化工具（Be JSON）', '2023-02-20 19:26:15', '2023-02-20 19:26:15');
INSERT INTO `collected_site` VALUES (16, 'SpringCloud中文网', 4, 'https://www.springcloud.cc/', 'https://www.springcloud.cc/favicon.ico', NULL, 'Spring Cloud中文网-官方文档中文版', '2023-02-20 19:27:25', '2023-02-20 19:27:25');
INSERT INTO `collected_site` VALUES (17, '程序员导航', 6, 'https://cxy521.com/', 'https://cxy521.com/favicon.ico', NULL, '序员一站式导航网站,在线工具', '2023-02-20 19:29:18', '2023-02-20 19:29:18');
INSERT INTO `collected_site` VALUES (18, '蛙蛙工具', 6, 'https://www.iamwawa.cn/', 'https://www.iamwawa.cn/favicon.ico', NULL, '蛙蛙工具', '2023-02-20 19:30:07', '2023-02-20 19:30:07');
INSERT INTO `collected_site` VALUES (19, '百度开发者搜索', 7, 'https://kaifa.baidu.com/', 'https://kaifa.baidu.com/favicon.ico', NULL, '百度开发者搜索引擎', '2023-02-20 19:32:04', '2023-02-20 19:32:04');
INSERT INTO `collected_site` VALUES (20, 'API文档', 6, 'https://www.apiref.com/', 'https://www.apiref.com/favicon.ico', NULL, 'API文档', '2023-02-20 19:33:55', '2023-02-20 19:33:55');
INSERT INTO `collected_site` VALUES (21, '菜鸟工具', 6, 'https://c.runoob.com/', 'https://c.runoob.com/favicon.ico', NULL, '菜鸟工具', '2023-02-20 19:34:41', '2023-02-20 19:34:41');
INSERT INTO `collected_site` VALUES (22, 'XML格式化', 5, 'https://xml.supfree.net/', 'https://xml.supfree.net/favicon.ico', NULL, 'XML格式化', '2023-02-20 19:35:28', '2023-02-20 19:35:28');
INSERT INTO `collected_site` VALUES (23, 'Java在线反编译', 5, 'http://www.javadecompilers.com/', 'http://www.javadecompilers.com/favicon.ico', NULL, 'Java在线反编译', '2023-02-20 19:37:01', '2023-02-20 19:37:01');
INSERT INTO `collected_site` VALUES (24, '清华大学开源软件镜像站', 3, 'https://mirrors.tuna.tsinghua.edu.cn/', 'https://mirrors.tuna.tsinghua.edu.cn/favicon.ico', NULL, '清华大学开源软件镜像站', '2023-02-20 19:37:50', '2023-02-20 19:37:50');
INSERT INTO `collected_site` VALUES (25, 'Nacos中文文档', 4, 'https://nacos.io/zh-cn/docs/open-api.html', 'https://nacos.io/favicon.ico', NULL, 'Nacos中文文档', '2023-02-20 19:39:37', '2023-02-20 19:39:37');
INSERT INTO `collected_site` VALUES (26, 'AI编程助手', 8, 'https://www.aicodehelper.com/', 'https://www.aicodehelper.com/favicon.ico', NULL, 'AI编程助手', '2023-02-20 19:40:39', '2023-02-20 19:40:39');
INSERT INTO `collected_site` VALUES (27, 'API space', 9, 'https://www.apispace.com/', 'https://www.apispace.com/favicon.ico', NULL, 'API space', '2023-02-20 19:41:43', '2023-02-20 19:41:43');
INSERT INTO `collected_site` VALUES (28, '天行数据API', 9, 'https://www.tianapi.com/', 'https://www.tianapi.com/favicon.ico', NULL, '天行数据API', '2023-02-20 19:42:39', '2023-02-20 19:42:39');
INSERT INTO `collected_site` VALUES (29, 'DigitalOcean', 1, 'https://www.digitalocean.com/', 'https://www.digitalocean.com/favicon.ico', NULL, 'DigitalOcean', '2023-02-20 20:01:54', '2023-02-20 20:01:54');

-- ----------------------------
-- Table structure for collected_site_type
-- ----------------------------
DROP TABLE IF EXISTS `collected_site_type`;
CREATE TABLE `collected_site_type`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collected_site_type
-- ----------------------------
INSERT INTO `collected_site_type` VALUES (8, 'AI');
INSERT INTO `collected_site_type` VALUES (9, 'API接口对接');
INSERT INTO `collected_site_type` VALUES (4, '学习平台');
INSERT INTO `collected_site_type` VALUES (6, '工具导航');
INSERT INTO `collected_site_type` VALUES (3, '常用工具');
INSERT INTO `collected_site_type` VALUES (5, '开发工具');
INSERT INTO `collected_site_type` VALUES (1, '技术社区');
INSERT INTO `collected_site_type` VALUES (7, '浏览器主页');
INSERT INTO `collected_site_type` VALUES (2, '视频专区');

-- ----------------------------
-- Table structure for junzi_manager
-- ----------------------------
DROP TABLE IF EXISTS `junzi_manager`;
CREATE TABLE `junzi_manager`  (
  `junzi_id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_pwd` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_datetime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_datetime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`junzi_id`, `login_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of junzi_manager
-- ----------------------------
INSERT INTO `junzi_manager` VALUES (1, '你家宝宝', 'fbb', '123456', '2023-01-13 15:36:40', '2023-01-13 15:36:40');
INSERT INTO `junzi_manager` VALUES (2, 'feng123', 'feng12345', '434324', '2023-02-14 15:17:50', '2023-02-14 15:17:50');
INSERT INTO `junzi_manager` VALUES (3, 'feng123', 'feng12345', '434324', '2023-02-14 15:25:03', '2023-02-14 15:25:03');

SET FOREIGN_KEY_CHECKS = 1;
