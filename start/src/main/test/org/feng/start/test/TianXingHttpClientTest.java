package org.feng.start.test;

import org.feng.navigation.Application;
import org.feng.navigation.domain.rpc.tianxing.domainobject.area.TianXingAreaSearchRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.area.TianXingAreaSearchResponse;
import org.feng.navigation.domain.rpc.tianxing.domainobject.robot.TianXingRobotRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.robot.TianXingRobotResponse;
import org.feng.navigation.domain.rpc.tianxing.domainobject.sentence.TianXingOneSentenceEverydayRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.sentence.TianXingOneSentenceEverydayResponse;
import org.feng.navigation.domain.rpc.tianxing.domainobject.weather.TianXingWeatherSearchRequest;
import org.feng.navigation.domain.rpc.tianxing.gateway.TianXingHttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * 天行数据-测试
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 14时20分
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TianXingHttpClientTest {

    @Resource
    private TianXingHttpClient tianXingHttpClient;

    @Test
    public void testTianXingSearchArea() {
        TianXingAreaSearchRequest request = new TianXingAreaSearchRequest();
        request.setArea("西安");
        TianXingAreaSearchResponse response = tianXingHttpClient.execute(request);

        if (response.isSuccess()) {
            List<TianXingAreaSearchResponse.TianXingAreaSearchResponseData> resultDataList = response.getResponseDataBody().getResultDataList();
            String areaId = "";
            for (TianXingAreaSearchResponse.TianXingAreaSearchResponseData data : resultDataList) {
                String province = data.getProvincecn();
                if ("陕西".equals(province)) {
                    areaId = data.getAreaid();
                    break;
                }
            }
            testTianXingWeatherSearch(areaId);
        }
    }

    @Test
    public void testOneSentenceEveryday() {
        TianXingOneSentenceEverydayRequest request = new TianXingOneSentenceEverydayRequest();
        TianXingOneSentenceEverydayResponse response = tianXingHttpClient.execute(request);
        System.out.println(response);
    }

    @Test
    public void testRobot() {
        TianXingRobotRequest request = new TianXingRobotRequest();
        request.setQuestion("西安天气");
        tianXingHttpClient.execute(request);
        request.setQuestion("讲个故事");
        tianXingHttpClient.execute(request);
        request.setQuestion("土味情话");
        tianXingHttpClient.execute(request);
        request.setQuestion("旅游景点");
        tianXingHttpClient.execute(request);
        request.setQuestion("彩虹屁");
        tianXingHttpClient.execute(request);
    }

    public void testTianXingWeatherSearch(String city) {
        if (city.startsWith("CN")) {
            city = city.replaceFirst("CN", "");
        }
        TianXingWeatherSearchRequest request = new TianXingWeatherSearchRequest();
        request.setCity(city);
        tianXingHttpClient.execute(request);
    }
}
