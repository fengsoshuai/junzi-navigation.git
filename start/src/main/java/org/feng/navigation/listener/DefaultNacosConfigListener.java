package org.feng.navigation.listener;

import com.alibaba.nacos.api.config.listener.Listener;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executor;

/**
 * Nacos 监听器
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月03日 17时31分
 */
@Slf4j
public class DefaultNacosConfigListener implements Listener {
    @Override
    public Executor getExecutor() {
        return null;
    }

    @Override
    public void receiveConfigInfo(String configInfo) {
        log.info("Nacos配置内容发生更改：{}", configInfo);
    }
}
