package org.feng.navigation.listener;

import com.alibaba.nacos.api.config.listener.Listener;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.common.util.GsonUtil;

import java.util.concurrent.Executor;

/**
 * Nacos 监听json数据
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月03日 18时33分
 */
@Slf4j
@AllArgsConstructor
public class JsonFileNacosConfigListener<T> implements Listener {

    private Class<T> instanceClass;

    @Override
    public Executor getExecutor() {
        return null;
    }

    @Override
    public void receiveConfigInfo(String configInfo) {
        boolean isJson = GsonUtil.validateJson(configInfo);
        if (!isJson) {
            log.error("Nacos配置错误，当前配置不是一个有效的JSON");
            return;
        }

        T instance = GsonUtil.fromJson(configInfo, instanceClass);
        log.info("将Nacos配置的json数据转换为实例：{}", instance);
    }
}
