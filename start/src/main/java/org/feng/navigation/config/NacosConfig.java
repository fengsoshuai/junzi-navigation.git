package org.feng.navigation.config;

import com.alibaba.cloud.nacos.NacosConfigProperties;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Properties;

/**
 * Nacos 配置
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月03日 16时40分
 */
@Slf4j
@Configuration
public class NacosConfig {
    @Resource
    private NacosConfigProperties nacosConfigProperties;

    @Bean
    public ConfigService configService() {
        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.SERVER_ADDR, nacosConfigProperties.getServerAddr());
        properties.setProperty(PropertyKeyConst.NAMESPACE, nacosConfigProperties.getNamespace());
        properties.setProperty(PropertyKeyConst.USERNAME, nacosConfigProperties.getUsername());
        properties.setProperty(PropertyKeyConst.PASSWORD, nacosConfigProperties.getPassword());
        ConfigService configServiceInstance;
        try {
            configServiceInstance = NacosFactory.createConfigService(properties);
        } catch (NacosException e) {
            log.error("初始化Nacos配置出错 {}", e.getMessage());
            return null;
        }
        return configServiceInstance;
    }
}
