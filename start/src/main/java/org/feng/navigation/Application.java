package org.feng.navigation;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * 启动器
 *
 * @author junzi
 */
@Slf4j
@EnableAspectJAutoProxy(exposeProxy = true)
@SpringBootApplication
@EnableDiscoveryClient
@ConfigurationPropertiesScan("org.feng.navigation")
@RefreshScope
@EnableCaching
public class Application implements ApplicationListener<ApplicationReadyEvent> {

    private static Environment environment;

    public static void main(String[] args) throws UnknownHostException {
        final ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(Application.class)
                .bannerMode(Banner.Mode.OFF)
                .run(args);

        Environment env = applicationContext.getEnvironment();
        environment = env;
        String contextPath = env.getProperty("server.servlet.context-path");
        log.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\thttp://localhost:{}{}\n\t" +
                        "External: \thttp://{}:{}{}\n\t" +
                        "Doc: \thttp://{}:{}{}/doc.html\n\t" +
                        "Actuator: \thttp://{}:{}{}/actuator\n\t" +
                        "Prometheus Actuator: \thttp://{}:{}{}/actuator/prometheus\n\t" +
                        "NacosConfig Actuator: \thttp://{}:{}{}/actuator/nacosconfig\n\t" +
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"), contextPath,
                InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"), contextPath,
                InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"), contextPath,
                InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"), contextPath,
                InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"), contextPath,
                InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"), contextPath
        );
    }

    @Override
    public void onApplicationEvent(@NotNull ApplicationReadyEvent applicationReadyEvent) {
        String applicationName = "";
        if (environment != null) {
            applicationName = Optional.ofNullable(environment.getProperty("spring.application.name")).orElse("");
        }
        log.info("{}项目启动完成", applicationName);
    }
}
