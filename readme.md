# 菌子导航

## 前言

本导航网站旨在对接多个三方接口，集成到导航系统中。目前正在对接的是有道文本翻译接口。

### 项目架构

参考并使用DDD领域模型思想，架构搭建使用了阿里的 COLA 项目。

https://gitee.com/fengsoshuai/COLA#

使用命令： mvn archetype:generate -DgroupId=org.feng.navigation -DartifactId=junzi-navigation -Dversion=1.0.0-SNAPSHOT
-Dpackage=org.feng.navigation -DarchetypeArtifactId=cola-framework-archetype-service -DarchetypeGroupId=com.alibaba.cola
-DarchetypeVersion=4.0.1

### spring-cloud 和 spring-boot 版本选择

参考：https://spring.io/projects/spring-cloud

spring-boot 选择 2.7.8 ，cloud选择 2021.0.5

### 使用到的组件（依赖）

| 组件           | 版本          |
|--------------|-------------|
| lombok       | 1.18.16     |
| okhttp       | 4.9.0       |
| mapstruct    | 1.4.2.Final |
| mybatis-plus | 3.5.3.1     |
| druid        | 1.2.15      |
| gson         | 2.9.1       |
| hutool-core  | 5.8.11      |
| caffeine     | 2.9.3       |
| swagger      | 3.0.0       |

### 架构分层

常规的COLA分层说明：https://blog.csdn.net/significantfrank/article/details/110934799

本项目的分层简述：

![架构](docs\架构.png)

## 项目基本功能

### 1 使用Nacos做配置中心

![20230119142832](docs\20230119142832.png)

配置内容如下：

```yaml
junzi:
  nav:
    url:
      youDao: https://openapi.youdao.com/api
    app:
      youDao:
        appKey: 在有道注册的账号的appkey
        appSecret: 在有道注册的账号的appSecret


# 数据源
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/junzi-navigation?useUnicode=true&serverTimezone=UTC
    username: root
    password: 123456
    type: com.alibaba.druid.pool.DruidDataSource
  # 支持post、delete等请求
  mvc:
    hiddenmethod:
      filter:
        enabled: true
# mybatis plus
mybatis-plus:
  # xml扫描，多个目录用逗号或者分号分隔（告诉mapper所对应的xml文件位置）
  mapper-locations: classpath*:mapper/**Mapper.xml
  # 以下配置均有默认值
  global-config:
    db-config:
      #主键类型  auto:"数据库ID自增" 1:"用户输入ID",2:"全局唯一ID (数字类型唯一ID)", 3:"全局唯一ID UUID";
      id-type: auto
      #字段策略 IGNORED:"忽略判断"  NOT_NULL:"非 NULL 判断")  NOT_EMPTY:"非空判断"
      field-strategy: NOT_EMPTY
      #数据库类型
      db-type: MYSQL
  configuration:
    # 是否开启自动驼峰命名规则映射:从数据库列名到Java属性驼峰命名的类似映射
    map-underscore-to-camel-case: true
    # 如果查询结果中包含空值的列，则 MyBatis 在映射的时候，不会映射这个字段
    call-setters-on-nulls: true
    # 这个配置会将执行的sql打印出来，在开发或测试的时候可以用
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  # 扫描实体
  type-aliases-package: org.feng.navigation.database.entity
```

### 2 logback日志

```xml

<configuration>
    <include resource="org/springframework/boot/logging/logback/defaults.xml"/>

    <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符 %F文件，%L行号-->
            <pattern>%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(${LOG_LEVEL_PATTERN:-%5p} ) %clr(${PID:- }){magenta}
                %clr(---){faint} %clr([%t]){faint} %clr(%logger{39}.%M\(%F:%L\)){cyan} %clr(:){faint}
                ![method=%X{method:-default},businessId=%X{businessId:-default}] %m%n${LOG_EXCEPTION_CONVERSION_WORD:-%wEx}
            </pattern>
            <charset>UTF-8</charset>
        </encoder>
    </appender>

    <!--rootLogger是默认的logger-->
    <root level="INFO">
        <appender-ref ref="CONSOLE"/>
    </root>

    <!--应用日志-->
    <logger name="org.feng.navigation" level="DEBUG"/>
</configuration>

```

### 3 mybatis-plus操作数据库

使用Mapper 和 ServiceImpl 作为常见的数据库功能实现。

并实现了 SQL 拦截 InnerInterceptor，打印出执行到的SQL（这一操作需要谨慎，线上部署可以去除）

### 4 Caffeine 缓存整合

使用 Spring Cache 中的 CacheManager管理缓存。

### 5 LocalDateTime 序列化&反序列化

增加序列化、反序列化的配置，将时间格式统一为 yyyy-MM-dd HH:mm:ss

并支持配置更改。

### 6 参数校验快速失败配置

在使用 Valid 注解进行参数校验时，默认会校验全部参数后，将错误信息才返回。

增加此配置后，只要发生了错误，就会直接返回，增加了校验效率与系统性能。

### 7 集成日志链路

使用拦截器 HandlerInterceptor 做了日志链路统一。

搭配着 MDC 和 logback 对关键词 “businessId” 进行输出。

使用效果是，同一请求的执行链路中，直接使用 lombok 打印日志，会将 businessId 打印出来。方便回溯和排查问题。

### 8 控制器参数切面

主要是在执行控制器的前、后打印数据。

执行前打印方法请求参数，执行后打印执行结果。

执行错误时，打印错误描述信息。

并且提供了 Check 接口的校验，如果当前参数DTO对象，实现了Check 接口，则会自动调用该接口的 check方法，对参数进行校验。

### 9 日志切面

搭配着MDC 和 lombok 将当前请求的接口对应的业务方法打印到日志中。

### 10 集成 Swagger + knife4j

集成了 3.0 版本的 Swagger 文档。

方便开发接口时的调试，测试。

### 11 全局异常拦截

对校验参数的异常进行捕获，对未知异常进行捕获并处理

### 12 Nacos 增加监听器

具体说明见博客：https://blog.csdn.net/FBB360JAVA/article/details/128878761

### 13 RestTemplate 集成 okhttp4

已经将httpclient 更改成为 okhttp。效率上快很多。

### 14 注册服务到Nacos

目标是另外新建项目后，可以用新建的项目 Feign 调用本项目。

目前新建项目已经搭建完毕：https://gitee.com/fengsoshuai/junzi-navigation-web.git

可以使用 junzi-navigation-web项目远程调用本项目。

### 15 集成自定义的redis starter

[参考博客SpringBoot 2.7.8 自定义 Starter &自动配置](https://blog.csdn.net/FBB360JAVA/article/details/128847565)

[参考博客Java 使用Reactive Redis](https://blog.csdn.net/FBB360JAVA/article/details/126569778)

在Nacos增加如下配置内容：

```yml
junzi:
	redis:
		# 表示spring容器需要注入redis相关配置
        enable: true
        # 业务
        business:
          # 业务1，可以自由配置key值
          get:
          	# 业务前缀
            businessPrefix: feng0-redis-
            # 业务过期时间
            businessExpireTime: 5
            # 时间单位
            expireTimeUnit: HOURS
            # 是否启用当前业务
            enable: true
          # 业务2，可以自由配置key值
          cancel:
            businessPrefix: feng1-redis-
            businessExpireTime: 3
            expireTimeUnit: HOURS
            enable: true
# 连接redis需要的配置参数
spring:
  redis:
    host: localhost
    port: 6379
    password: feng123
```



## 项目业务功能

### 1 集成有道文本翻译接口

目前已经对接完毕，支持中、英翻译。

![20230119145459](docs\20230119145459.png)



### 2 集成天行数据

在 https://www.tianapi.com/ 注册账号，并对接里边的接口。

目前对接了：

+ [天行数据新闻接口频道ID](www.tianapi.com/article/118)



### 3 枚举生成器

运行对应的前端项目 vue-back-navigation 中的 junzi-navigation-vue分支的代码，可以得到一个前端页面。

![20230219092314](docs\20230219092314.png)

在本地选择你自己的csv文件，指定类名、包名、枚举key值对应的索引（索引从0开始计数）

比如测试时使用 demo.csv文件内容：

```
1,线下热敏,线下热敏是一种物流
5,淘宝,淘淘他
7,拼多多,品谷博的武器二带我去
```

最终生成的Demo.java 文件的内容是：

```java
package org.feng;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * TODO
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月11日 14时00分
 */
@Getter
@AllArgsConstructor
public enum Demo {

    /**
     * 线下热敏：1
     */
    XIAN_XIA_RE_MIN("1", "线下热敏", "线下热敏是一种物流"),
    /**
     * 淘宝：5
     */
    TAO_BAO("5", "淘宝", "淘淘他"),
    /**
     * 拼多多：7
     */
    PIN_DUO_DUO("7", "拼多多", "品谷博的武器二带我去"),
    ;
    /**
     *  TODO 待完善
     */
    private final Integer key;
    /**
     *  TODO 待完善
     */
    private final String desc;

    public static Demo of(Integer key) {
        if (key == null) {
            throw new IllegalArgumentException("key不能为空的啦");
        }
        return Arrays.stream(values())
                .filter(element -> element.key.equals(key))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("key不存在的啦"));
    }
}

```



### 4 网站录入&分页查看

增加了网站录入，提供网站名，ico以及logo（logo后面增加上传文件后，才实际用到），网站url等的录入。

分页查询网站信息。（网站录入后台已完成，前端未写）列出网站功能已经完成。

![20230219135025](docs\20230219135025.png)



### 5 天气预报

正在接入中。目前后台已经完成，并测试通过，vue前端正在码代码。

## 项目部署与运行

### 1 Idea中运行

在下载项目源码后，使用idea的maven插件功能，在图中先 clean，再 install。

随后运行 start 模块中的启动类即可。

注意 profile 的指定，目前使用的是dev环境。

![20230129211940](docs\20230129211940.png)

### 2 打包在windows的cmd中运行

先打包，注意使用clean 和 package 插件。
![20230129213414](docs\20230129213414.png)

等待打包成功后，在 start 模块中找到新打好的包。

![20230129213602](docs\20230129213602.png)

随后在java11的运行环境中运行该jar包（注意要指定编码格式，cmd默认会是gbk）：

```java
java -Dfile.encoding=utf-8 -jar start-1.0.0-SNAPSHOT.jar
```

这里调整编码的操作是因为第一次运行时，读取到nacos中的配置内容，是带乱码的，导致程序启动不起来。因为nacos默认是utf-8编码的。

### 3 在Linux后台运行

```
nohup java -jar start-1.0.0-SNAPSHOT.jar > /usr/software/junzi/junzi.log &
```

并输出日志到`junzi.log`中。

### 4 使用Prometheus & Grafana 监控jvm

首先是安装好这俩软件。

安装步骤可以参考：[Prometheus & Grafana 的安装](https://blog.csdn.net/FBB360JAVA/article/details/127815529)

在配置文件`prometheus.yml`中增加如下内容

```
  - job_name: "junzi-navigation"
    metrics_path: "/junzi/actuator/prometheus"
    static_configs:
      - targets: ["localhost:18080"]
```

随后重启prometheus。

登陆到Grafana中，打开配置增加数据源。选择 prometheus类型的数据源。

![20230314220303](docs\20230314220303.png)

随后输入你的 prometheus 的地址和端口，比如你安装的路径是 localhost:9090，就输入它。随后点击保存

![20230314220614](docs\20230314220614.png)

然后导入一个Jvm的监控面板，选择你刚刚加上的数据源就可以啦。

![20230314221029](docs\20230314221029.png)

比如我这里选择了 4701 号的面板。效果如下：

![20230314221240](docs\20230314221240.png)

![20230314221356](docs\20230314221356.png)

