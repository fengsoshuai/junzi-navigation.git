package org.feng.navigation.domain.tianxing.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 输入类型：目前仅支持文本
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月03日 14时22分
 */
@Getter
@AllArgsConstructor
public enum TianXingRobotInputTypeEnum {
    TEXT(0, "文本");
    private final int type;
    private final String msg;
}
