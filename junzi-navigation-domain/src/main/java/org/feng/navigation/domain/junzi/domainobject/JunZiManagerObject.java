package org.feng.navigation.domain.junzi.domainobject;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月19日 12时23分
 */
@Data
public class JunZiManagerObject {
    private Integer junziId;

    private String name;

    private String loginName;

    private LocalDateTime createDatetime;
}
