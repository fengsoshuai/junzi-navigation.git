package org.feng.navigation.domain.rpc.tianxing.domainobject.sentence;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingResponse;

/**
 * 每日一句响应体
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月02日 19时08分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingOneSentenceEverydayResponse extends BaseTianXingResponse {

    @SerializedName("result")
    private TianXingOneSentenceEverydayResponseBody result;

    @Data
    public static class TianXingOneSentenceEverydayResponseBody {
        /**
         * 	时间
         */
        @SerializedName("date")
        private String date;
        /**
         * 图片作者
         */
        @SerializedName("imgauthor")
        private String imgauthor;
        /**
         * 图片路径
         */
        @SerializedName("imgurl")
        private String imgurl;
        /**
         * ID
         */
        @SerializedName("oneid")
        private Long oneid;
        /**
         * 句子
         */
        @SerializedName("word")
        private String word;
        /**
         * 句子来源
         */
        @SerializedName("wordfrom")
        private String wordfrom;
    }
}
