package org.feng.navigation.domain.generator.domainobject;

import lombok.Data;
import org.feng.navigation.common.util.TimeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 枚举生成对象，可以将参数转换为模板需要的数据格式
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 19时35分
 */
@Data
public class EnumGeneratorObject {

    /**
     * 作者
     */
    private String author;
    /**
     * 版本
     */
    private String version;
    /**
     * yyyy年MM月dd日 HH时mm分 格式的时间
     */
    private String date;
    /**
     * 枚举类中的变量
     */
    private List<FieldObject> fieldObjects;
    /**
     * key值对应的数据
     */
    private FieldObject keyObject;
    /**
     * 枚举类名
     */
    private String customEnumClassName;
    /**
     * 生成的枚举的内容
     */
    private String generatedContent;

    private List<EnumBodyObject> enumBodyList;

    /**
     * 完整包名
     */
    private String fullPackageName;

    public EnumGeneratorObject() {
        this.init();
    }

    private void init() {
        this.author = "junzi";
        this.version = "V1.0";
        this.date = TimeUtil.formatCurrent();
    }

    public Map<String, Object> toMap() {
        Map<String, Object> templateParamMap = new HashMap<>(16);

        templateParamMap.put("customEnumClassName", this.customEnumClassName);
        templateParamMap.put("author", this.author);
        templateParamMap.put("version", this.version);
        templateParamMap.put("date", this.date);
        templateParamMap.put("fullPackageName", this.fullPackageName);
        templateParamMap.put("enumBodyObjects", this.enumBodyList);

        FieldObject code = new FieldObject();
        // 设置key值的类型为字符串
        code.setFieldType(String.class);
        code.setFieldName("key");
        this.keyObject = code;
        this.keyObject.setNonExistMessage("key不存在的啦");
        this.keyObject.setNonNullMessage("key不能为空的啦");
        templateParamMap.put("keyObject", keyObject);

        FieldObject desc = new FieldObject();
        desc.setFieldType(String.class);
        desc.setFieldName("desc");

        fieldObjects = new ArrayList<>(List.of(code, desc));
        templateParamMap.put("fieldObjects", fieldObjects);
        return templateParamMap;
    }
}
