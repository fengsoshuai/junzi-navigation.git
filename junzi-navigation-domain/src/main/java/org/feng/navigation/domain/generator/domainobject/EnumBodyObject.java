package org.feng.navigation.domain.generator.domainobject;

import lombok.Data;

/**
 * 枚举体：含枚举内容的名字，参数，文档注释
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月10日 18时53分
 */
@Data
public class EnumBodyObject {
    /**
     * 枚举体，含参数
     */
    private String body;
    /**
     * 枚举的文档注释
     */
    private String doc = " TODO 待完善";
}
