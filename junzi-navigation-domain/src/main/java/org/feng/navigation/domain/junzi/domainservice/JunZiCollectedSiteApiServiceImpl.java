package org.feng.navigation.domain.junzi.domainservice;

import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteObject;
import org.feng.navigation.domain.junzi.gateway.JunZiCollectedSiteDbService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 菌子站点服务接口实现
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 20时11分
 */
@Service
public class JunZiCollectedSiteApiServiceImpl implements JunZiCollectedSiteApiService {

    @Resource
    private JunZiCollectedSiteDbService junZiCollectedSiteDbService;

    @Override
    public List<JunZiCollectedSiteObject> listJunZiCollectedSites(int current, int size) {
        return junZiCollectedSiteDbService.listJunZiCollectedSites(current, size);
    }

    @Override
    public boolean addJunZiCollectedSite(JunZiCollectedSiteObject junZiCollectedSiteObject) {
        return junZiCollectedSiteDbService.insertCollectedSite(junZiCollectedSiteObject);
    }
}
