package org.feng.navigation.domain.tianxing.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.feng.navigation.domain.tianxing.enums.TianXingWeatherSearchTypeEnum;

/**
 * 天行数据-天气预报领域对象实体
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 15时18分
 */
@Data
public class TianXingWeatherSearchObject {
    private static final String CN = "CN";

    /**
     * 地区天气ID
     */
    @Setter(AccessLevel.NONE)
    private String city;
    /**
     * 类型：实时或7天
     */
    private TianXingWeatherSearchTypeEnum typeEnum;
    /**
     * 错误信息
     */
    private String errorMessage;
    /**
     * 天气预报结果
     */
    private TianXingWeatherSearchResultData resultData;

    public void setCity(String city) {
        if (city != null && city.startsWith(CN)) {
            city = city.replaceFirst(CN, "");
        }
        this.city = city;
    }

    public boolean hasError() {
        return resultData == null || errorMessage != null;
    }
}
