package org.feng.navigation.domain.rpc.tianxing.domainobject.sentence;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.common.util.TimeUtil;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingRequest;

/**
 * 每日一句请求体
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月02日 19时04分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingOneSentenceEverydayRequest extends BaseTianXingRequest<TianXingOneSentenceEverydayResponse> {
    /**
     * 是否随机
     */
    @SerializedName("rand")
    private Integer rand;
    /**
     * 指定时间，比如 2023-03-3
     */
    @SerializedName("date")
    private String date;

    public TianXingOneSentenceEverydayRequest() {
        this.rand = 0;
        this.date = TimeUtil.formatCurrentDate();
    }

    @Override
    public String getPath() {
        return "/one/index";
    }
}
