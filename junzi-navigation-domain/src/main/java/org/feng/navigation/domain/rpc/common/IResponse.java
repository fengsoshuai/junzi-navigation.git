package org.feng.navigation.domain.rpc.common;

import org.feng.navigation.common.util.GsonUtil;

/**
 * 响应接口：所有响应体需要实现它
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 14时18分
 */
public interface IResponse {

    /**
     * 设置响应码
     *
     * @param code 响应码
     */
    default void setCode(String code) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * 设置响应信息
     *
     * @param message 响应信息
     */
    default void setMessage(String message) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * 接口或者业务是否成功。
     * 所有子类必须实现该方法。
     *
     * @return 接口调用是否成功
     */
    default boolean isSuccess() {
        return true;
    }

    /**
     * 序列化对象为Json格式
     *
     * @return 序列化对象为Json格式
     */
    default String toJson() {
        return GsonUtil.toJson(this);
    }
}
