package org.feng.navigation.domain.qyapi.factory;

import lombok.NonNull;
import org.feng.navigation.domain.qyapi.enums.MessageType;
import org.feng.navigation.domain.rpc.qyapi.domainobject.BaseEnterpriseWeChatRobotRequest;
import org.feng.navigation.domain.rpc.qyapi.domainobject.EnterpriseWechatRobotMarkdownRequest;
import org.feng.navigation.domain.rpc.qyapi.domainobject.EnterpriseWechatRobotTextRequest;

/**
 * BaseEnterpriseWeChatRobotRequest 的工厂
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 15时38分
 */
public class EnterpriseWechatRobotFactory {

    public static BaseEnterpriseWeChatRobotRequest newEnterpriseWeChatRobotRequest(@NonNull MessageType messageType, @NonNull String content) {
        switch (messageType) {
            case MARKDOWN:
                return new EnterpriseWechatRobotMarkdownRequest().setMarkdown(new EnterpriseWechatRobotMarkdownRequest.MarkdownBody().setContent(content));
            default:
                return new EnterpriseWechatRobotTextRequest().setText(new EnterpriseWechatRobotTextRequest.TextBody().setContent(content));
        }
    }
}
