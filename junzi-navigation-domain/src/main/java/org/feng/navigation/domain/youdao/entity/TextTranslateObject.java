package org.feng.navigation.domain.youdao.entity;

import lombok.Data;
import org.feng.navigation.domain.youdao.enums.TextTranslateSupportLanguageEnum;

/**
 * 文本翻译领域对象实体
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 22时08分
 */
@Data
public class TextTranslateObject {
    private TextTranslateSupportLanguageEnum from;
    private TextTranslateSupportLanguageEnum to;
    private String itemText;

    /**
     * 翻译结果
     */
    private String translation;
    /**
     * 错误信息
     */
    private String errorMessage;
}
