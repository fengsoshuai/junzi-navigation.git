package org.feng.navigation.domain.frame;

/**
 * 网关：对于领域层domain而言，对外提供的服务的接口（其他接口需要继承它）
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月16日 14时57分
 */
public interface Gateway {
}
