package org.feng.navigation.domain.rpc.common;

import org.feng.navigation.common.util.GsonUtil;

/**
 * 请求接口：所有请求体需要实现它
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 14时17分
 */
public interface IRequest<R extends IResponse> {
    /**
     * 获取请求路径或者接口请求方法名
     *
     * @return 方法名、请求路径
     */
    default String getPath() {
        throw new UnsupportedOperationException("暂不支持的方法");
    }

    /**
     * 获得一个响应类的类型，每一个请求体会对应一个响应类型
     *
     * @return Response 的子类
     */
    Class<R> getResponseClass();

    /**
     * 序列化对象为Json格式
     *
     * @return 序列化对象为Json格式
     */
    default String toJson() {
        return GsonUtil.toJson(this);
    }
}
