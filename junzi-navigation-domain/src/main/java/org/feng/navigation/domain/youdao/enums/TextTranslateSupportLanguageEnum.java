package org.feng.navigation.domain.youdao.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 文本翻译支持的语言枚举
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 22时10分
 */
@Getter
@AllArgsConstructor
public enum TextTranslateSupportLanguageEnum {
    /**
     * 中文
     */
    CHINESE("zh-CHS", "中文"),
    /**
     * 英文
     */
    ENGLISH("en", "英文"),
    /**
     * 自动识别
     */
    AUTO("auto", "自动识别"),
    ;

    private final String code;
    private final String desc;

    public static TextTranslateSupportLanguageEnum of(String code) {
        return Arrays.stream(values())
                .filter(ele -> ele.code.equals(code))
                .findAny().orElse(AUTO);
    }
}
