package org.feng.navigation.domain.rpc.tianxing.domainobject;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.feng.navigation.domain.rpc.common.IResponse;

import java.util.Objects;

/**
 * 天行数据接口响应基类
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 13时42分
 */
@Data
public class BaseTianXingResponse implements IResponse {
    /**
     * 状态码（code => 200表示成功计费）
     */
    @SerializedName("code")
    private Integer code;
    /**
     * 错误信息
     */
    @SerializedName("msg")
    private String msg;

    @Override
    public boolean isSuccess() {
        return Objects.equals(200, code);
    }
}
