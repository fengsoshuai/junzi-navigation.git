package org.feng.navigation.domain.rpc.tianxing.domainobject.weather;

import lombok.*;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingRequest;

/**
 * 天行-天气预报查询请求体
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 14时54分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingWeatherSearchRequest extends BaseTianXingRequest<TianXingWeatherSearchResponse> {
    /**
     * 地区天气ID
     */
    private String city;
    /**
     * 类型：实时选“1”，7天选“7”<br>
     * 本系统默认为实时
     */
    private Integer type;

    public TianXingWeatherSearchRequest() {
        this.type = 1;
    }

    @Override
    public String getPath() {
        return "/tianqi/index";
    }
}
