package org.feng.navigation.domain.rpc.tianxing.domainservice;

import org.feng.navigation.domain.tianxing.entity.TianXingAllNewsObject;
import org.feng.navigation.domain.tianxing.entity.TianXingAreaSearchObject;
import org.feng.navigation.domain.tianxing.entity.TianXingRobotObject;
import org.feng.navigation.domain.tianxing.entity.TianXingWeatherSearchObject;

/**
 * 天行数据接口服务<br>
 * <ul>
 *     <li>分类新闻</li>
 *     <li>地区查询</li>
 *     <li>天气预报</li>
 * </ul>
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 13时45分
 */
public interface TianXingApiService {

    /**
     * 天行数据-分类新闻api服务
     *
     * @param tianXingAllNewsObject 天行数据-分类新闻的领域对象实体
     */
    void tianXingAllNews(TianXingAllNewsObject tianXingAllNewsObject);

    /**
     * 天行数据-地区查询
     *
     * @param tianXingAreaSearchObject 天行数据-地区查询的领域对象实体
     */
    void tianXingAreaSearch(TianXingAreaSearchObject tianXingAreaSearchObject);

    /**
     * 天行数据-天气预报
     *
     * @param tianXingWeatherSearchObject 天行数据-天气预报的领域对象实体
     */
    void tianXingWeatherSearch(TianXingWeatherSearchObject tianXingWeatherSearchObject);

    /**
     * 天行数据-聊天机器人
     * @param tianXingRobotObject 天行数据-聊天机器人的领域对象实体
     */
    void tianXingChatRobot(TianXingRobotObject tianXingRobotObject);
}
