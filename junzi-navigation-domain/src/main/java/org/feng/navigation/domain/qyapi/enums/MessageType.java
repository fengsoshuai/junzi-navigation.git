package org.feng.navigation.domain.qyapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 自定义机器人支持文本（text）、markdown（markdown）、图片（image）、图文（news）四种消息类型
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月27日 14时53分
 */
@Getter
@AllArgsConstructor
public enum MessageType {
    TEXT("text", "文本"),
    MARKDOWN("markdown", "markdown"),
    IMAGE("image", "图片"),
    NEWS("news", "图文");
    private final String code;
    private final String desc;

    public static MessageType of(String code) {
        if (code == null) {
            return TEXT;
        }
        return Arrays.stream(values())
                .filter(ele -> ele.code.equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("不存在该消息类型"));
    }
}
