package org.feng.navigation.domain.junzi.domainservice;

import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteObject;

import java.util.List;

/**
 * 菌子站点服务接口<br>
 * <li>分页列出站点</li>
 * <li>增加一个站点</li>
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 20时11分
 */
public interface JunZiCollectedSiteApiService {
    /**
     * 分页列出站点
     *
     * @param current 当前页
     * @param size    每页大小
     * @return 站点列表
     */
    List<JunZiCollectedSiteObject> listJunZiCollectedSites(int current, int size);

    /**
     * 增加一个站点
     *
     * @param junZiCollectedSiteObject 站点对象
     * @return 是否增加成功，true表示成功
     */
    boolean addJunZiCollectedSite(JunZiCollectedSiteObject junZiCollectedSiteObject);
}
