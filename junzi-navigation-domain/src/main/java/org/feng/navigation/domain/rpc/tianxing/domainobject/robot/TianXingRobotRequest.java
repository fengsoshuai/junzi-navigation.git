package org.feng.navigation.domain.rpc.tianxing.domainobject.robot;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingRequest;

/**
 * 天行机器人请求体
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月02日 19时31分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingRobotRequest extends BaseTianXingRequest<TianXingRobotResponse> {
    /**
     * 提问
     */
    @SerializedName("question")
    private String question;
    /**
     * 用户唯一身份ID，方便上下文关联
     */
    @SerializedName("uniqueid")
    private String uniqueId;
    /**
     * 工作模式，宽松模式0（回答率高）、精确模式1（相关性高）、私有模式2（只从私有词库中回答）
     */
    @SerializedName("mode")
    private Integer mode;
    /**
     * 私有词库匹配模式，完整匹配0[默认]、智能匹配1、模糊匹配2、结尾匹配3、开头匹配4
     */
    @SerializedName("priv")
    private Integer priv;
    /**
     * 输入类型，文本0、语音1、人脸图片2
     */
    @SerializedName("restype")
    private Integer inputType;

    @Override
    public String getPath() {
        return "/robot/index";
    }
}
