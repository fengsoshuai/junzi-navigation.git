package org.feng.navigation.domain.tianxing.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 机器人回复内容类型
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月03日 14时20分
 */
@Getter
@AllArgsConstructor
public enum TianXingRobotReplyTypeEnum {
    TEXT("text", "文本");
    private final String type;
    private final String msg;
}
