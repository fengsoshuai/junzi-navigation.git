package org.feng.navigation.domain.router;

import java.lang.annotation.*;

/**
 * 路由器：根据方法上的请求参数类型，路由为不同的业务；<br>classRouterKey、stringRouterKey不能同时为空
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 14时57分
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Router {
    Class<?> DEFAULT_CLASS_ROUTER_KEY = Object.class;

    Class<?> classRouterKey() default Object.class;

    String stringRouterKey() default "";

    /**
     * 方法描述
     *
     * @return 描述内容
     */
    String desc() default "";
}
