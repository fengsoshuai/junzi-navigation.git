package org.feng.navigation.domain.generator.domainservice;

import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.domain.generator.domainobject.EnumGeneratorObject;
import org.feng.navigation.domain.generator.gateway.GeneratorClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 生成器API服务实现
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 19时19分
 */
@Slf4j
@Service
public class GeneratorApiServiceImpl implements GeneratorApiService {

    @Resource(name = "customEnumAutoGeneratorClient")
    private GeneratorClient customEnumAutoGeneratorClient;

    @Override
    public void generateCustomEnum(EnumGeneratorObject enumGeneratorObject) {
        customEnumAutoGeneratorClient.addTemplateParam(enumGeneratorObject.toMap());
        String generatedContent = customEnumAutoGeneratorClient.process();
        enumGeneratorObject.setGeneratedContent(generatedContent);
    }
}
