package org.feng.navigation.domain.tianxing.entity;

import lombok.Data;
import org.feng.navigation.domain.tianxing.enums.TianXingRobotInputTypeEnum;
import org.feng.navigation.domain.tianxing.enums.TianXingRobotModeEnum;
import org.feng.navigation.domain.tianxing.enums.TianXingRobotReplyTypeEnum;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月03日 14时14分
 */
@Data
public class TianXingRobotObject {
    /**
     * 唯一ID
     */
    private String uniqueId;
    private String question;
    private TianXingRobotModeEnum robotMode;
    private TianXingRobotInputTypeEnum inputType;

    /**
     * 机器人回复内容
     */
    private String reply;
    private TianXingRobotReplyTypeEnum replyType;
    /**
     * 错误信息
     */
    private String errorMessage;

    public boolean hasError() {
        return reply == null || errorMessage != null;
    }
}
