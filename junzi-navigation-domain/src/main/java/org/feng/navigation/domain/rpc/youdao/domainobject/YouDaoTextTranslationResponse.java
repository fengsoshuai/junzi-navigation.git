package org.feng.navigation.domain.rpc.youdao.domainobject;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 有道文本翻译响应体
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 20时56分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class YouDaoTextTranslationResponse extends BaseYouDaoResponse {
    @SerializedName("errorCode")
    private String errorCode;
    @SerializedName("query")
    private String query;
    @SerializedName("translation")
    private List<String> translationList;

    /**
     * 错误信息
     */
    private transient String errorMessage;

    @Override
    public boolean isSuccess() {
        return "0".equals(errorCode);
    }
}
