package org.feng.navigation.domain.rpc.qyapi.domainobject;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NonNull;
import org.feng.navigation.common.util.GsonUtil;
import org.feng.navigation.domain.qyapi.enums.MessageType;

/**
 * 企业微信机器人请求体基类
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月27日 14时50分
 */
@Data
public abstract class BaseEnterpriseWeChatRobotRequest {
    @SerializedName("msgtype")
    private final String messageType;

    /**
     * wechat 群对应机器人的key，不能序列化，不能为空
     */
    private transient String key;

    public BaseEnterpriseWeChatRobotRequest(@NonNull MessageType messageType) {
        this.messageType = messageType.getCode();
    }

    public String toJson() {
        return GsonUtil.toJson(this);
    }
}
