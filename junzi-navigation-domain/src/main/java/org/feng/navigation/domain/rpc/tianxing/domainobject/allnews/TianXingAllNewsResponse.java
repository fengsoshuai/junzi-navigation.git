package org.feng.navigation.domain.rpc.tianxing.domainobject.allnews;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingResponse;

import java.util.List;

/**
 * 天行数据-分类新闻响应体
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 13时56分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingAllNewsResponse extends BaseTianXingResponse {

    /**
     * 响应的业务数据
     */
    @SerializedName("result")
    private TianXingAllNewsResponseData result;

    @Data
    public static class TianXingAllNewsResponseData {
        @SerializedName("curpage")
        private Integer curpage;
        @SerializedName("allnum")
        private Integer allnum;
        @SerializedName("newslist")
        private List<TianXingAllNewsResponseList> newsList;
    }

    @Data
    public static class TianXingAllNewsResponseList {
        @SerializedName("id")
        private String id;
        @SerializedName("ctime")
        private String ctime;
        @SerializedName("title")
        private String title;
        @SerializedName("description")
        private String description;
        @SerializedName("source")
        private String source;
        @SerializedName("picUrl")
        private String picUrl;
        @SerializedName("url")
        private String url;
    }
}
