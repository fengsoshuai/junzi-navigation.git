package org.feng.navigation.domain.rpc.qyapi.domainservice;

import org.feng.navigation.domain.qyapi.entity.EnterpriseWechatRobotObject;

/**
 * 企业微信机器人接口服务<br>
 * <ul>
 *     <li>发送消息</li>
 * </ul>
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 15时17分
 */
public interface EnterpriseWechatRobotApiService {

    void send(EnterpriseWechatRobotObject enterpriseWechatRobotObject);
}
