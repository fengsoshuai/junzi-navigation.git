package org.feng.navigation.domain.generator.domainobject;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

/**
 * 变量对象
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月07日 22时33分
 */
@Data
@Slf4j
public class FieldObject {

    /**
     * 变量名
     */
    private String fieldName;

    /**
     * 变量类型
     */
    private Class<?> fieldType;
    /**
     * 变量的文档注释
     */
    private String fieldDoc = " TODO 待完善";
    /**
     * 变量类型的简单名字，比如变量类型<code>fieldType</code>是 <code>java.lang.String</code> 时，该值为 <code>String</code>
     */
    @Setter(AccessLevel.NONE)
    private String simpleFieldType;

    /**
     * key不为空异常时，报的提示词
     */
    private String nonNullMessage;
    /**
     * key不存在时，报的提示词
     */
    private String nonExistMessage;

    public void setFieldType(Class<?> fieldType) {
        if (!SUPPORTED_FIELD_TYPE_SET.contains(fieldType)) {
            log.warn("不支持的字段类型{}，目前只支持整数、字符串，将自动为您切换为字符串类型", fieldType);
            fieldType = String.class;
        }
        this.fieldType = fieldType;
        this.simpleFieldType = fieldType.getSimpleName();
    }

    /**
     * 当前已经支持的字段变量类型
     */
    private static final Set<Class<?>> SUPPORTED_FIELD_TYPE_SET = Set.of(String.class, Integer.class);
}
