package org.feng.navigation.domain.rpc.youdao.domainservice;

import org.feng.navigation.domain.youdao.entity.TextTranslateObject;

/**
 * 有道API接口服务<br>
 * <ul>
 *     <li>文本翻译</li>
 * </ul>
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 14时39分
 */
public interface YouDaoApiService {

    /**
     * 文本翻译
     *
     * @param textTranslateObject 文本翻译领域对象实体
     */
    void textTranslate(TextTranslateObject textTranslateObject);
}
