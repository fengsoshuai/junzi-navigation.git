package org.feng.navigation.domain.rpc.tianxing.gateway;

import org.feng.navigation.domain.frame.Gateway;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingResponse;

/**
 * 天行数据-接口http请求
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 13时42分
 */
public interface TianXingHttpClient extends Gateway {

    <R extends BaseTianXingResponse> R execute(BaseTianXingRequest<R> request);
}
