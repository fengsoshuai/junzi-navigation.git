package org.feng.navigation.domain.rpc.youdao.domainobject;

import org.feng.navigation.domain.rpc.common.IResponse;

/**
 * 有道响应体基类
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 14时34分
 */
public class BaseYouDaoResponse implements IResponse {
}
