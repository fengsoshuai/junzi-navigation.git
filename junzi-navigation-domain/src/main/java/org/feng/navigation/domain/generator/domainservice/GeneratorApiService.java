package org.feng.navigation.domain.generator.domainservice;

import org.feng.navigation.domain.generator.domainobject.EnumGeneratorObject;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 19时19分
 */
public interface GeneratorApiService {

    void generateCustomEnum(EnumGeneratorObject enumGeneratorObject);
}
