package org.feng.navigation.domain.tianxing.entity;

import lombok.Data;
import org.feng.navigation.domain.tianxing.enums.TianXingAllNewsTypeEnum;

import java.time.LocalDate;
import java.util.List;

/**
 * 天行数据-分类新闻的领域对象实体
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 15时37分
 */
@Data
public class TianXingAllNewsObject {
    /**
     * 新闻频道ID
     */
    private TianXingAllNewsTypeEnum tianXingAllNewsTypeEnum;
    /**
     * 新闻数量
     */
    private Integer newsCount;

    /**
     * 响应的新闻列表
     */
    private List<AllNewsData> newsList;
    /**
     * 响应的错误信息
     */
    private String errorMessage;

    @Data
    public static class AllNewsData {
        /**
         * 时间
         */
        private LocalDate date;
        /**
         * 标题
         */
        private String title;
        /**
         * 新闻来源
         */
        private String source;
        /**
         * 配图地址
         */
        private String picUrl;
        /**
         * 新闻地址
         */
        private String url;
    }
}
