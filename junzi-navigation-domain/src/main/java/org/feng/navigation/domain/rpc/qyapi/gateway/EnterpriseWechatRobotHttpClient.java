package org.feng.navigation.domain.rpc.qyapi.gateway;

import org.feng.navigation.domain.rpc.qyapi.domainobject.BaseEnterpriseWeChatRobotRequest;

/**
 * 企业微信机器人-接口http请求
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月27日 14时47分
 */
public interface EnterpriseWechatRobotHttpClient {
    String ROBOT_URL = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=";

    /**
     * 通过机器人发送消息到群
     *
     * @param request 请求体
     */
    void send(BaseEnterpriseWeChatRobotRequest request);
}
