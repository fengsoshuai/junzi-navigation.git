package org.feng.navigation.domain.tianxing.entity;

import lombok.Data;

/**
 * 天行数据-地区查询的领域对象
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 14时39分
 */
@Data
public class TianXingAreaSearchObject {

    /**
     * 地区
     */
    private String area;
    /**
     * 省（处理响应时使用，最初结果有多个，通过它来确定具体是哪个城市）
     */
    private String province;
    /**
     * 地区天气ID（响应结果）
     */
    private String areaId;
    /**
     * 错误信息
     */
    private String errorMessage;

    public boolean hasError() {
        return areaId == null || errorMessage != null;
    }
}
