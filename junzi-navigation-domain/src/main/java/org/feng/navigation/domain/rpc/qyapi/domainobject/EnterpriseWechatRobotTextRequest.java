package org.feng.navigation.domain.rpc.qyapi.domainobject;


import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.feng.navigation.domain.qyapi.enums.MessageType;

import java.util.List;

/**
 * 文本类型消息，组装如下Json的报文：
 * <pre>
 * {
 *     "msgtype": "text",
 *     "text": {
 *         "content": "广州今日天气：29度，大部分多云，降雨概率：60%",
 *         "mentioned_list":["wangqing","@all"],
 *         "mentioned_mobile_list":["13800001111","@all"]
 *     }
 * }
 * </pre>
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月27日 14时55分
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class EnterpriseWechatRobotTextRequest extends BaseEnterpriseWeChatRobotRequest {
    @Getter
    @Setter
    @SerializedName("text")
    private TextBody text;

    public EnterpriseWechatRobotTextRequest() {
        super(MessageType.TEXT);
    }

    @Accessors(chain = true)
    @Data
    public static class TextBody {
        /**
         * 文本内容，最长不超过2048个字节，必须是utf8编码
         */
        @SerializedName("content")
        private String content;
        /**
         * userid的列表，提醒群中的指定成员(@某个成员)，@all表示提醒所有人，如果开发者获取不到userid，可以使用mentioned_mobile_list
         */
        @SerializedName("mentioned_list")
        private List<String> mentionedList;
        /**
         * 手机号列表，提醒手机号对应的群成员(@某个成员)，@all表示提醒所有人
         */
        @SerializedName("mentioned_mobile_list")
        private List<String> mentionedMobileList;
    }
}
