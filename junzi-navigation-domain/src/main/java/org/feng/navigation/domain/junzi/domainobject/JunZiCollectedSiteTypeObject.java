package org.feng.navigation.domain.junzi.domainobject;

import lombok.Data;

/**
 * 站点类型
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 20时50分
 */
@Data
public class JunZiCollectedSiteTypeObject {
    private Integer id;
    /**
     * 类型名称
     */
    private String type;
}
