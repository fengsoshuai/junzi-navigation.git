package org.feng.navigation.domain.rpc.tianxing.domainservice;

import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.common.util.StringUtil;
import org.feng.navigation.common.util.TimeUtil;
import org.feng.navigation.domain.rpc.tianxing.domainobject.allnews.TianXingAllNewsRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.allnews.TianXingAllNewsResponse;
import org.feng.navigation.domain.rpc.tianxing.domainobject.area.TianXingAreaSearchRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.area.TianXingAreaSearchResponse;
import org.feng.navigation.domain.rpc.tianxing.domainobject.robot.TianXingRobotRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.robot.TianXingRobotResponse;
import org.feng.navigation.domain.rpc.tianxing.domainobject.weather.TianXingWeatherSearchRequest;
import org.feng.navigation.domain.rpc.tianxing.domainobject.weather.TianXingWeatherSearchResponse;
import org.feng.navigation.domain.rpc.tianxing.gateway.TianXingHttpClient;
import org.feng.navigation.domain.tianxing.entity.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 天行数据接口服务实现
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 13时45分
 */
@Slf4j
@Service
public class TianXingApiServiceImpl implements TianXingApiService {

    @Resource
    private TianXingHttpClient tianXingHttpClient;

    @Override
    public void tianXingAllNews(TianXingAllNewsObject tianXingAllNewsObject) {

        TianXingAllNewsRequest request = new TianXingAllNewsRequest();
        // 设置新闻数量
        request.setNum(tianXingAllNewsObject.getNewsCount());
        // 设置新闻频道
        request.setCol(tianXingAllNewsObject.getTianXingAllNewsTypeEnum().getChannelId());

        // 请求天行新闻接口
        TianXingAllNewsResponse response = tianXingHttpClient.execute(request);
        // 响应失败
        if (!response.isSuccess()) {
            tianXingAllNewsObject.setErrorMessage(response.getMsg());
            return;
        }
        // 处理响应
        TianXingAllNewsResponse.TianXingAllNewsResponseData result = response.getResult();
        if (Objects.nonNull(result)) {
            List<TianXingAllNewsResponse.TianXingAllNewsResponseList> newsList = result.getNewsList();
            if (CollectionUtil.isNotEmpty(newsList)) {
                List<TianXingAllNewsObject.AllNewsData> allNewsDataList = new ArrayList<>();
                tianXingAllNewsObject.setNewsList(allNewsDataList);
                for (TianXingAllNewsResponse.TianXingAllNewsResponseList news : newsList) {
                    TianXingAllNewsObject.AllNewsData allNewsData = new TianXingAllNewsObject.AllNewsData();
                    allNewsDataList.add(allNewsData);
                    allNewsData.setUrl(news.getUrl());
                    allNewsData.setTitle(news.getTitle());
                    allNewsData.setPicUrl(news.getPicUrl());
                    allNewsData.setSource(news.getSource());
                    allNewsData.setDate(TimeUtil.parse(StringUtil.substring(news.getCtime(), 10)));
                }
            }
        }
    }

    @Override
    public void tianXingAreaSearch(TianXingAreaSearchObject tianXingAreaSearchObject) {
        TianXingAreaSearchRequest request = new TianXingAreaSearchRequest();
        request.setArea(tianXingAreaSearchObject.getArea());
        TianXingAreaSearchResponse response = tianXingHttpClient.execute(request);
        // 处理失败
        if (!response.isSuccess()) {
            tianXingAreaSearchObject.setErrorMessage(response.getMsg());
            return;
        }
        List<TianXingAreaSearchResponse.TianXingAreaSearchResponseData> resultDataList = response.getResponseDataBody().getResultDataList();
        String areaId = "";
        for (TianXingAreaSearchResponse.TianXingAreaSearchResponseData data : resultDataList) {
            String province = data.getProvincecn();
            // 匹配省：比如“陕西省”包含了“陕西”
            if (tianXingAreaSearchObject.getProvince() != null && tianXingAreaSearchObject.getProvince().contains(province)) {
                areaId = data.getAreaid();
                break;
            }
        }
        // 主要是获取地区天气ID
        tianXingAreaSearchObject.setAreaId(areaId);
    }

    @Override
    public void tianXingWeatherSearch(TianXingWeatherSearchObject tianXingWeatherSearchObject) {
        TianXingWeatherSearchRequest request = new TianXingWeatherSearchRequest();
        request.setCity(tianXingWeatherSearchObject.getCity());
        request.setType(tianXingWeatherSearchObject.getTypeEnum().getType());
        TianXingWeatherSearchResponse response = tianXingHttpClient.execute(request);
        // 处理失败
        if (!response.isSuccess() || response.getResponseData() == null) {
            tianXingWeatherSearchObject.setErrorMessage(response.getMsg());
            return;
        }
        TianXingWeatherSearchResultData weatherSearchResultData = response.getResponseData().transferToResultData();
        tianXingWeatherSearchObject.setResultData(weatherSearchResultData);
    }

    @Override
    public void tianXingChatRobot(TianXingRobotObject tianXingRobotObject) {
        TianXingRobotRequest request = new TianXingRobotRequest();
        request.setUniqueId(tianXingRobotObject.getUniqueId());
        request.setQuestion(tianXingRobotObject.getQuestion());
        TianXingRobotResponse response = tianXingHttpClient.execute(request);
        // 处理失败
        if (!response.isSuccess() || response.getResult() == null) {
            tianXingRobotObject.setErrorMessage(response.getMsg());
            return;
        }
        // 处理成功响应结果
        tianXingRobotObject.setReply(response.getResult().getReply());
    }
}
