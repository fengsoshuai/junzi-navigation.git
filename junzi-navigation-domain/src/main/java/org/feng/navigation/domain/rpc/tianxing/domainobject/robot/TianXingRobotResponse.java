package org.feng.navigation.domain.rpc.tianxing.domainobject.robot;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingResponse;

/**
 * 天行机器人响应体
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月02日 19时32分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingRobotResponse extends BaseTianXingResponse {

    @SerializedName("result")
    private TianXingRobotResponseBody result;

    @Data
    public static class TianXingRobotResponseBody {
        @SerializedName("reply")
        private String reply;
        @SerializedName("datatype")
        private String dataType;
    }
}
