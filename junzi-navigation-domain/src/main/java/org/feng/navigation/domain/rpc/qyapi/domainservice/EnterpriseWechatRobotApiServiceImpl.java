package org.feng.navigation.domain.rpc.qyapi.domainservice;

import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.domain.qyapi.entity.EnterpriseWechatRobotObject;
import org.feng.navigation.domain.qyapi.entity.QyApiRobotKeyObject;
import org.feng.navigation.domain.qyapi.factory.EnterpriseWechatRobotFactory;
import org.feng.navigation.domain.rpc.qyapi.domainobject.BaseEnterpriseWeChatRobotRequest;
import org.feng.navigation.domain.rpc.qyapi.gateway.EnterpriseWechatRobotHttpClient;
import org.feng.navigation.domain.rpc.qyapi.gateway.QyApiRobotDbService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 企业微信机器人接口服务实现
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 15时27分
 */
@Slf4j
@Service
public class EnterpriseWechatRobotApiServiceImpl implements EnterpriseWechatRobotApiService {

    @Resource
    private EnterpriseWechatRobotHttpClient enterpriseWechatRobotHttpClient;
    @Resource
    private QyApiRobotDbService qyApiRobotDbService;

    @Override
    public void send(EnterpriseWechatRobotObject enterpriseWechatRobotObject) {
        BaseEnterpriseWeChatRobotRequest request = EnterpriseWechatRobotFactory
                .newEnterpriseWeChatRobotRequest(enterpriseWechatRobotObject.getMessageType(), enterpriseWechatRobotObject.getContent());

        // 查库：找对应ID的机器人key信息
        QyApiRobotKeyObject oneRobot = qyApiRobotDbService.findOneRobot(enterpriseWechatRobotObject.getKey());
        if (oneRobot == null) {
            throw new IllegalArgumentException("不存在机器人key：" + enterpriseWechatRobotObject.getKey());
        }
        request.setKey(oneRobot.getRobotKey());
        enterpriseWechatRobotHttpClient.send(request);
    }
}
