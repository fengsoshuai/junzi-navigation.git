package org.feng.navigation.domain.rpc.tianxing.domainobject.weather;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingResponse;
import org.feng.navigation.domain.tianxing.entity.TianXingWeatherSearchResultData;

import java.util.List;

/**
 * 天行-天气预报查询响应体
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 14时54分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingWeatherSearchResponse extends BaseTianXingResponse {

    @SerializedName("result")
    private TianXingWeatherSearchResponseData responseData;

    @Data
    public static class TianXingWeatherSearchResponseData {

        private List<Object> alarmlist;
        private String aqi;
        private String area;
        private String areaid;
        private String date;
        private String highest;
        private String humidity;
        private String lowest;
        private String moondown;
        private String moonrise;
        private String pcpn;
        private String province;
        private String quality;
        private String real;
        private String sunrise;
        private String sunset;
        private String tips;
        private String uvIndex;
        private String vis;
        private String weather;
        private String weathercode;
        private String weatherimg;
        private String week;
        private String wind;
        private String windsc;
        private String windspeed;

        public TianXingWeatherSearchResultData transferToResultData() {
            TianXingWeatherSearchResultData tianXingWeatherSearchResultData = new TianXingWeatherSearchResultData();

            tianXingWeatherSearchResultData.setArea(getArea());
            tianXingWeatherSearchResultData.setAreaid(getAreaid());
            tianXingWeatherSearchResultData.setDate(getDate());
            tianXingWeatherSearchResultData.setHighest(getHighest());
            tianXingWeatherSearchResultData.setLowest(getLowest());
            tianXingWeatherSearchResultData.setProvince(getProvince());
            tianXingWeatherSearchResultData.setQuality(getQuality());
            tianXingWeatherSearchResultData.setReal(getReal());
            tianXingWeatherSearchResultData.setWeather(getWeather());
            tianXingWeatherSearchResultData.setWeathercode(getWeathercode());
            tianXingWeatherSearchResultData.setWeatherimg(getWeatherimg());
            tianXingWeatherSearchResultData.setWeek(getWeek());
            tianXingWeatherSearchResultData.setWind(getWind());
            return tianXingWeatherSearchResultData;
        }
    }
}
