package org.feng.navigation.domain.tianxing.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 天行数据新闻接口频道ID 枚举<br>
 * 参考：<a href="https://www.tianapi.com/article/118">天行数据新闻接口频道ID</a>
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 15时38分
 */
@Getter
@AllArgsConstructor
public enum TianXingAllNewsTypeEnum {

    /**
     * 宠物新闻：46
     */
    CHONG_WU_XIN_WEN(46, "宠物新闻", "宠物及相关产业新闻资讯"),
    /**
     * 电竞资讯：45
     */
    DIAN_JING_ZI_XUN(45, "电竞资讯", "电子竞技新闻资讯接口"),
    /**
     * 女性新闻：43
     */
    NU_XING_XIN_WEN(43, "女性新闻", "新浪女性新闻频道"),
    /**
     * 垃圾分类新闻：42
     */
    LA_JI_FEN_LEI_XIN_WEN(42, "垃圾分类新闻", "垃圾分类新闻资讯接口"),
    /**
     * 环保资讯：41
     */
    HUAN_BAO_ZI_XUN(41, "环保资讯", "人民网环保新闻资讯"),
    /**
     * 影视资讯：40
     */
    YING_SHI_ZI_XUN(40, "影视资讯", "影视资讯新闻接口"),
    /**
     * 币圈资讯：39
     */
    BI_QUAN_ZI_XUN(39, "币圈资讯", "数字货币行业新闻资讯"),
    /**
     * 汉服新闻：38
     */
    HAN_FU_XIN_WEN(38, "汉服新闻", "民族文化汉服新闻资讯"),
    /**
     * 房产新闻：37
     */
    FANG_CHAN_XIN_WEN(37, "房产新闻", "房产写字楼新闻资讯"),
    /**
     * 科学探索：36
     */
    KE_XUE_TAN_SUO(36, "科学探索", "探索宇宙和科学的真相"),
    /**
     * 汽车新闻：35
     */
    QI_CHE_XIN_WEN(35, "汽车新闻", "汽车行业新闻资讯接口"),
    /**
     * 互联网资讯：34
     */
    HU_LIAN_WANG_ZI_XUN(34, "互联网资讯", "互联网资讯新闻接口"),
    /**
     * 动漫资讯：33
     */
    DONG_MAN_ZI_XUN(33, "动漫资讯", "全网热点动漫资讯，带你了解二次元世界"),
    /**
     * 财经新闻：32
     */
    CAI_JING_XIN_WEN(32, "财经新闻", "财经资讯，了解身边的经济大事"),
    /**
     * 游戏资讯：31
     */
    YOU_XI_ZI_XUN(31, "游戏资讯", "游戏资讯接口，提供游戏每日精选新闻"),
    /**
     * CBA新闻：30
     */
    CBAXIN_WEN(30, "CBA新闻", "CBA新闻API、中国男子职业篮球赛资讯等"),
    /**
     * 人工智能：29
     */
    REN_GONG_ZHI_NENG(29, "人工智能", "AI人工智能行业相关新闻资讯API"),
    /**
     * 区块链新闻：28
     */
    QU_KUAI_LIAN_XIN_WEN(28, "区块链新闻", "区块链行业相关新闻资讯API"),
    /**
     * 军事新闻：27
     */
    JUN_SHI_XIN_WEN(27, "军事新闻", "军事资讯API、军情动态、科技发展等"),
    /**
     * 足球新闻：26
     */
    ZU_QIU_XIN_WEN(26, "足球新闻", "国足资讯API、国足明星动态等"),
    /**
     * 创业新闻：24
     */
    CHUANG_YE_XIN_WEN(24, "创业新闻", "互联网行业新闻API、创新、创业人物动态"),
    /**
     * 移动互联：23
     */
    YI_DONG_HU_LIAN(23, "移动互联", "移动互联网行业资讯API"),
    /**
     * IT资讯：22
     */
    ITZI_XUN(22, "IT资讯", "IT行业相关新闻资讯API"),
    /**
     * VR科技：21
     */
    VRKE_JI(21, "VR科技", "VR虚拟现实相关新闻资讯API"),
    /**
     * NBA新闻：20
     */
    NBAXIN_WEN(20, "NBA新闻", "NBA动态API、篮球赛等"),
    /**
     * 苹果新闻：19
     */
    PIN_GUO_XIN_WEN(19, "苹果新闻", "Apple产品动态API，果粉、教程帮助"),
    /**
     * 旅游资讯：18
     */
    LU_YOU_ZI_XUN(18, "旅游资讯", "旅游、周边、景点新闻资讯API"),
    /**
     * 健康知识：17
     */
    JIAN_KANG_ZHI_SHI(17, "健康知识", "健康知识、养生、中西医资讯API"),
    /**
     * 美女图片：11
     */
    MEI_NU_TU_PIAN(11, "美女图片", "美女图片、大家都懂"),
    /**
     * 奇闻异事：14
     */
    QI_WEN_YI_SHI(14, "奇闻异事", "世界奇闻资讯API、民间趣事、灵异事件等"),
    /**
     * 科技新闻：13
     */
    KE_JI_XIN_WEN(13, "科技新闻", "信息科技行业新闻API、物理科技"),
    /**
     * 体育新闻：12
     */
    TI_YU_XIN_WEN(12, "体育新闻", "国内外体育API、体育明星动态等"),
    /**
     * 娱乐新闻：10
     */
    YU_LE_XIN_WEN(10, "娱乐新闻", "娱乐新闻API、明星花边、探班、娱乐活动等"),
    /**
     * 国际新闻：8
     */
    GUO_JI_XIN_WEN(8, "国际新闻", "国际新闻API接口服务"),
    /**
     * 国内新闻：7
     */
    GUO_NEI_XIN_WEN(7, "国内新闻", "国内新闻API接口服务"),
    /**
     * 社会新闻：5
     */
    SHE_HUI_XIN_WEN(5, "社会新闻", "社会新闻API接口服务");

    /**
     * 频道ID
     */
    private final Integer channelId;
    /**
     * 资讯类型
     */
    private final String informationType;
    /**
     * 说明
     */
    private final String desc;

    public static TianXingAllNewsTypeEnum of(Integer channelId) {
        if (channelId == null) {
            throw new IllegalArgumentException("天行数据新闻接口频道ID不能为空");
        }
        return Arrays.stream(values())
                .filter(ele -> ele.channelId.equals(channelId))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("不存在该天行数据新闻接口频道ID"));
    }
}
