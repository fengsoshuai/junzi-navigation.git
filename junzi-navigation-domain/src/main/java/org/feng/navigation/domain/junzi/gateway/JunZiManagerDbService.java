package org.feng.navigation.domain.junzi.gateway;

import org.feng.navigation.domain.frame.Gateway;
import org.feng.navigation.domain.junzi.domainobject.JunZiManagerObject;

import java.util.List;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月19日 12时21分
 */
public interface JunZiManagerDbService extends Gateway {
    List<JunZiManagerObject> listJunZiManager();
}
