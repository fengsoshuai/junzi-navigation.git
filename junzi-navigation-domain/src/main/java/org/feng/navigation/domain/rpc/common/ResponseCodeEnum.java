package org.feng.navigation.domain.rpc.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 响应码枚举
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 14时26分
 */
@AllArgsConstructor
@Getter
public enum ResponseCodeEnum {
    /**
     * 成功
     */
    SUCCESS("0", "成功"),

    /**
     * 错误
     */
    ERROR("-1", "错误");
    /**
     * 响应码
     */
    private final String code;
    /**
     * 响应信息
     */
    private final String message;
}
