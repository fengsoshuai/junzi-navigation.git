package org.feng.navigation.domain.rpc.tianxing.domainobject.area;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingRequest;

/**
 * 天行地区搜索请求体
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 14时00分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingAreaSearchRequest extends BaseTianXingRequest<TianXingAreaSearchResponse> {
    /**
     * 地区名称，比如 “西安”
     */
    @SerializedName("area")
    private String area;
    /**
     * 返回数量，取值1-50
     */
    @SerializedName("num")
    private String num;
    /**
     * 翻页
     */
    @SerializedName("page")
    private String page;

    @Override
    public String getPath() {
        return "/citylookup/index";
    }
}
