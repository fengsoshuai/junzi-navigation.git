package org.feng.navigation.domain.junzi.domainobject;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 菌子-站点对象
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 19时50分
 */
@Data
public class JunZiCollectedSiteObject {
    private Integer id;
    private String name;
    /**
     * 站点类型
     */
    private Integer siteType;
    private String url;
    private String ico;
    private String logo;
    /**
     * 站点备注、描述
     */
    private String remark;
    /**
     * 站点登记时间
     */
    private LocalDateTime createdTime;
    /**
     * 上一次数据更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 站点类型对象
     */
    private JunZiCollectedSiteTypeObject junZiCollectedSiteTypeObject;
}
