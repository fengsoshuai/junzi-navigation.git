package org.feng.navigation.domain.junzi.gateway;

import org.feng.navigation.domain.frame.Gateway;
import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteObject;

import java.util.List;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 09时46分
 */
public interface JunZiCollectedSiteDbService extends Gateway {

    /**
     * 增加一条网站数据
     *
     * @param junZiCollectedSiteObject 网站对象
     * @return 数据库影响的条数
     */
    boolean insertCollectedSite(JunZiCollectedSiteObject junZiCollectedSiteObject);

    /**
     * 分页查询站点数据
     *
     * @param current 当前页
     * @param size 每页大小
     * @return 数据列表
     */
    List<JunZiCollectedSiteObject> listJunZiCollectedSites(int current, int size);
}
