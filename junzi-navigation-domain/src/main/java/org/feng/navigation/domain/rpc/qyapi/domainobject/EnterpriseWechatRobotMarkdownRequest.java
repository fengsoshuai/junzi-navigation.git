package org.feng.navigation.domain.rpc.qyapi.domainobject;


import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.feng.navigation.domain.qyapi.enums.MessageType;

/**
 * markdown类型消息，组装如下Json的报文：
 * <pre>
 * {
 *     "msgtype": "markdown",
 *     "markdown": {
 *         "content": "实时新增用户反馈<font color=\"warning\">132例</font>，请相关同事注意。\n
 *          >类型:<font color=\"info\">用户反馈</font>
 *          >普通用户反馈:<font color=\"comment\">117例</font>
 *          >VIP用户反馈:<font color=\"warning\">15例</font>"
 *     }
 * }
 * </pre>
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月27日 14时58分
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class EnterpriseWechatRobotMarkdownRequest extends BaseEnterpriseWeChatRobotRequest {
    @Getter
    @Setter
    @SerializedName("markdown")
    private EnterpriseWechatRobotMarkdownRequest.MarkdownBody markdown;

    public EnterpriseWechatRobotMarkdownRequest() {
        super(MessageType.MARKDOWN);
    }

    @Data
    @Accessors(chain = true)
    public static class MarkdownBody {
        /**
         * markdown内容，最长不超过4096个字节，必须是utf8编码
         */
        @SerializedName("content")
        private String content;
    }
}
