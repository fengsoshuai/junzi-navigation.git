package org.feng.navigation.domain.rpc.youdao.gateway;

import org.feng.navigation.domain.frame.Gateway;
import org.feng.navigation.domain.rpc.youdao.domainobject.YouDaoTextTranslationRequest;
import org.feng.navigation.domain.rpc.youdao.domainobject.YouDaoTextTranslationResponse;

/**
 * 有道智云-AI开放平台接口<br>
 * <a href="https://ai.youdao.com/">有道智云-AI开放平台</a>
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月10日 10时19分
 */
public interface YouDaoHttpClient extends Gateway {
    /**
     * 执行接口请求
     *
     * @param request 请求体
     * @return 响应结果
     *
    <R extends BaseYouDaoResponse> R execute(BaseYouDaoRequest<R> request); */

    YouDaoTextTranslationResponse executeTextTranslate(YouDaoTextTranslationRequest request);
}
