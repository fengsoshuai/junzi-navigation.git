package org.feng.navigation.domain.rpc.tianxing.domainobject;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.feng.navigation.domain.rpc.common.IRequest;

import java.lang.reflect.ParameterizedType;

/**
 * 天行数据接口请求基类
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 13时42分
 */
@Data
public class BaseTianXingRequest<R extends BaseTianXingResponse> implements IRequest<R> {
    /**
     * API密钥：在 <a href="https://www.tianapi.com/">天行数据</a>注册后获得
     */
    @SerializedName("key")
    private String key;

    @SuppressWarnings("unchecked")
    @Override
    public Class<R> getResponseClass() {
        return (Class<R>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
}
