package org.feng.navigation.domain.router;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 路由工厂类
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 15时04分
 */
@Slf4j
public abstract class AbstractRouterFactory {

    protected final Map<Class<?>, Method> classMethodMap;

    protected final Map<String, Method> stringMethodMap;

    public AbstractRouterFactory() {
        classMethodMap = new HashMap<>(16);
        stringMethodMap = new HashMap<>(16);
    }

    @PostConstruct
    private void init() {
        Method[] declaredMethods = this.getClass().getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            if (declaredMethod.isAnnotationPresent(Router.class)) {
                Router router = declaredMethod.getAnnotation(Router.class);
                Class<?> classRouterKey = router.classRouterKey();
                String stringRouterKey = router.stringRouterKey();
                if (StrUtil.isNotEmpty(stringRouterKey)) {
                    stringMethodMap.put(stringRouterKey, declaredMethod);
                    continue;
                }
                if (Router.DEFAULT_CLASS_ROUTER_KEY.equals(classRouterKey)) {
                    log.warn("当前注解没有设置routerKey");
                    continue;
                }
                classMethodMap.put(classRouterKey, declaredMethod);
            }
        }
        // 记录日志
        classMethodMap.forEach((key, value) -> {
            log.info("初始化路由classMethodMap：key={}, value={}", key.getSimpleName(), value.getName());
        });
        // 记录日志
        stringMethodMap.forEach((key, value) -> {
            log.info("初始化路由stringMethodMap：key={}, value={}", key, value.getName());
        });
    }

    /**
     * 在子类中，定义多个方法，使用Router注解表示不同的业务处理
     *
     * @param businessParam 业务参数对象，它的类型作为key
     * @param resultClass   需要转换的响应类型
     * @param <T>           响应类型
     * @return 响应结果
     */
    @SuppressWarnings("all")
    public <T> T execute(Object businessParam, Class<T> resultClass) {
        Method method = classMethodMap.get(businessParam.getClass());
        Object result;
        try {
            method.setAccessible(true);
            result = method.invoke(this, businessParam);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        return (T) result;
    }

    /**
     * 在子类中，定义多个方法，使用Router注解表示不同的业务处理
     *
     * @param businessParam 业务参数对象
     * @param resultClass   需要转换的响应类型
     * @param routerKey     key
     * @param <T>           响应类型
     * @return 响应结果
     */
    @SuppressWarnings("all")
    public <T> T execute(Object businessParam, String routerKey, Class<T> resultClass) {
        Method method = classMethodMap.get(routerKey);
        if (method == null) {
            return null;
        }
        Object result;
        try {
            method.setAccessible(true);
            result = method.invoke(this, businessParam);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        return (T) result;
    }
}
