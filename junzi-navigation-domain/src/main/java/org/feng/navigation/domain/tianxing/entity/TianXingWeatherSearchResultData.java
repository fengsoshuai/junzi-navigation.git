package org.feng.navigation.domain.tianxing.entity;

import lombok.Data;

/**
 * 天气预报结果数据
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 15时45分
 */
@Data
public class TianXingWeatherSearchResultData {
    /**
     * 结果地区（市/区/县）
     */
    private String area;
    /**
     * 城市天气ID
     */
    private String areaid;
    /**
     * 日期
     */
    private String date;
    /**
     * 最高温（日间温度）
     */
    private String highest;
    /**
     * 最低温（夜间温度）
     */
    private String lowest;
    /**
     * 所属省份
     */
    private String province;
    /**
     * 空气质量提示（七天无此字段）
     */
    private String quality;
    /**
     * 实时气温（七天仅为参考）
     */
    private String real;
    /**
     * 实时天气（七天为早晚变化）
     */
    private String weather;
    /**
     * 天气代码
     */
    private String weathercode;
    /**
     * 天气图标
     */
    private String weatherimg;
    /**
     * 星期
     */
    private String week;
    /**
     * 风向（方位）
     */
    private String wind;
}
