package org.feng.navigation.domain.qyapi.entity;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import org.feng.navigation.domain.qyapi.enums.MessageType;

/**
 * 企业微信机器人的领域对象
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 15时25分
 */
@Data
@Accessors(chain = true)
public class EnterpriseWechatRobotObject {

    private final MessageType messageType;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 机器人的key的ID
     */
    private Integer key;

    public EnterpriseWechatRobotObject(@NonNull MessageType messageType) {
        this.messageType = messageType;
    }
}
