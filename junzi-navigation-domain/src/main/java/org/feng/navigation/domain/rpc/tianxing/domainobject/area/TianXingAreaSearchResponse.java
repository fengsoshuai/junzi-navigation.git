package org.feng.navigation.domain.rpc.tianxing.domainobject.area;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingResponse;

import java.util.List;

/**
 * 天行地区搜索响应体
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 14时01分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingAreaSearchResponse extends BaseTianXingResponse {


    @SerializedName("result")
    private TianXingAreaSearchResponseDataBody responseDataBody;

    @Data
    public static class TianXingAreaSearchResponseDataBody {
        /**
         * 响应的业务数据
         */
        @SerializedName("list")
        private List<TianXingAreaSearchResponseData> resultDataList;
    }

    @Data
    public static class TianXingAreaSearchResponseData {
        /**
         * 地区行政代码
         */
        private String adcode;
        /**
         * 地区名称
         */
        private String areacn;
        /**
         * 地区拼音
         */
        private String areaen;
        /**
         * 地区天气ID
         */
        private String areaid;
        /**
         * 地区类型，地市级1、区县级2
         */
        private Long areatype;
        /**
         * 归属地市名称
         */
        private String citycn;
        /**
         * 归属地市拼音
         */
        private String cityen;
        /**
         * 地区纬度
         */
        private Double latitude;
        /**
         * 地区经度
         */
        private Double longitude;
        /**
         * 归属省份名称
         */
        private String provincecn;
        /**
         * 归属省份拼音
         */
        private String provinceen;
    }
}
