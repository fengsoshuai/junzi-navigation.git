package org.feng.navigation.domain.tianxing.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 天行天气预报类型
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 15时22分
 */
@Getter
@AllArgsConstructor
public enum TianXingWeatherSearchTypeEnum {
    /**
     * 实时天气：1
     */
    REAL_TIME_WEATHER(1, "实时天气"),
    /**
     * 7日天气：7
     */
    SEVEN_DAY_weather(7, "7日天气");

    private final Integer type;
    private final String desc;

    public static TianXingWeatherSearchTypeEnum of(Integer key) {
        return Arrays.stream(values())
                .filter(element -> element.type.equals(key))
                .findAny()
                .orElse(REAL_TIME_WEATHER);
    }
}
