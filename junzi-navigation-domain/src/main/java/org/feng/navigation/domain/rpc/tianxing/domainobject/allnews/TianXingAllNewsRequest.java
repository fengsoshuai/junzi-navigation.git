package org.feng.navigation.domain.rpc.tianxing.domainobject.allnews;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.feng.navigation.domain.rpc.tianxing.domainobject.BaseTianXingRequest;

/**
 * 天行数据-分类新闻接口对应的请求体
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 13时55分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TianXingAllNewsRequest extends BaseTianXingRequest<TianXingAllNewsResponse> {
    /**
     * 返回数量1-50，默认10（必填）
     */
    @SerializedName("num")
    private Integer num;
    /**
     * 新闻频道对应的ID（必填）
     */
    @SerializedName("col")
    private Integer col;
    /**
     * 翻页（非必填）
     */
    @SerializedName("page")
    private Integer page;
    /**
     * 随机获取（非必填）
     */
    @SerializedName("rand")
    private Integer rand;
    /**
     * 检索关键词（非必填）
     */
    @SerializedName("word")
    private String word;

    @Override
    public String getPath() {
        return "/allnews/index";
    }
}
