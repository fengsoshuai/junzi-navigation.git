package org.feng.navigation.domain.tianxing.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 宽松模式0（回答率高）、精确模式1（相关性高）、私有模式2（只从私有词库中回答）
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月03日 14时21分
 */
@Getter
@AllArgsConstructor
public enum TianXingRobotModeEnum {
    LOOSE_MODE(0, "宽松模式0（回答率高）"),
    PRECISE_MODE(1, "精确模式1（相关性高）"),
    PRIVATE_MODE(2, "私有模式2（只从私有词库中回答）"),
    ;
    private final int mode;
    private final String msg;
}
