package org.feng.navigation.domain.junzi.gateway;

import org.feng.navigation.domain.junzi.domainobject.JunZiCollectedSiteTypeObject;

import java.util.List;

/**
 * 站点类型数据库服务
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时06分
 */
public interface JunZiCollectedSiteTypeDbService {
    /**
     * 查找所有的站点类型
     *
     * @return 站点类型对象
     */
    List<JunZiCollectedSiteTypeObject> listJunZiCollectedSiteTypes();
}
