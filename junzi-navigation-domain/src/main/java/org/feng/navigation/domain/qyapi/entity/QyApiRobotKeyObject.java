package org.feng.navigation.domain.qyapi.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 17时09分
 */
@Data
@Accessors(chain = true)
public class QyApiRobotKeyObject {
    /**
     * 机器人key
     */
    private String robotKey;

    private Integer id;
    /**
     * 群名+机器人名
     */
    private String robotName;
}
