package org.feng.navigation.domain.generator.gateway;

import java.util.Map;

/**
 * 生成器客户端接口
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 16时52分
 */
public interface GeneratorClient {
    /**
     * 获取生成的内容
     *
     * @return 字符串，生成的内容
     */
    String process();

    void addTemplateParam(String paramKey, Object paramValue);

    void addTemplateParam(Map<String, Object> templateParamMap);
}
