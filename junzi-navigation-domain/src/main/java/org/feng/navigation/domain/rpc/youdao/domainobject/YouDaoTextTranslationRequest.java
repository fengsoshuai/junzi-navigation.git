package org.feng.navigation.domain.rpc.youdao.domainobject;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 有道-文本翻译请求体
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 20时56分
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class YouDaoTextTranslationRequest extends BaseYouDaoRequest<YouDaoTextTranslationResponse> {

    public YouDaoTextTranslationRequest() {
        init();
    }

    private void init() {
        this.signType = "v3";
    }

    @SerializedName("q")
    private String q;
    @SerializedName("from")
    private String from;
    @SerializedName("to")
    private String to;
    @SerializedName("appKey")
    private String appKey;
    @SerializedName("salt")
    private String salt;
    @SerializedName("sign")
    private String sign;
    @SerializedName("signType")
    private String signType;
    @SerializedName("curtime")
    private String currentTime;
    @SerializedName("ext")
    private String ext;
    @SerializedName("voice")
    private String voice;
    @SerializedName("strict")
    private String strict;
    @SerializedName("vocabld")
    private String vocabld;
    @SerializedName("domain")
    private String domain;
    @SerializedName("rejectFallback")
    private String rejectFallback;
}
