package org.feng.navigation.domain.rpc.youdao.domainobject;

import org.feng.navigation.domain.rpc.common.IRequest;

import java.lang.reflect.ParameterizedType;

/**
 * 有道请求体基类
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 14时33分
 */
public class BaseYouDaoRequest<R extends BaseYouDaoResponse> implements IRequest<R> {
    @SuppressWarnings("unchecked")
    @Override
    public Class<R> getResponseClass() {
        return (Class<R>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
}
