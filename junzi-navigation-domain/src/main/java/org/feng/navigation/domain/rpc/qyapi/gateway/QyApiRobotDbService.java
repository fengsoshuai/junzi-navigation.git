package org.feng.navigation.domain.rpc.qyapi.gateway;

import org.feng.navigation.domain.frame.Gateway;
import org.feng.navigation.domain.qyapi.entity.QyApiRobotKeyObject;

import java.util.List;

/**
 * 企业微信机器人数据库服务接口
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 17时04分
 */
public interface QyApiRobotDbService extends Gateway {

    /**
     * 通过ID查找一个机器人key的数据
     *
     * @param id 主键id
     * @return 含key的对象
     */
    QyApiRobotKeyObject findOneRobot(Integer id);

    List<QyApiRobotKeyObject> findAll();
}
