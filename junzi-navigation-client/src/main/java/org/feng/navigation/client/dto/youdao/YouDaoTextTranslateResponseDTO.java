package org.feng.navigation.client.dto.youdao;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.feng.navigation.client.dto.ApiDTO;

/**
 * 有道文本翻译响应结果DTO
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 22时40分
 */
@Data
@Accessors(chain = true, fluent = false)
@ApiModel
public class YouDaoTextTranslateResponseDTO implements ApiDTO {
    private static final long serialVersionUID = 987298318781258896L;
    @ApiModelProperty(value = "翻译结果")
    private String translation;
}
