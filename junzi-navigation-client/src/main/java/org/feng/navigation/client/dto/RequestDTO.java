package org.feng.navigation.client.dto;

/**
 * 请求体DTO：所有请求体应该实现它
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月01日 09时03分
 */
public interface RequestDTO extends ApiDTO, Check {
}
