package org.feng.navigation.client.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 单个响应结果的响应
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年01月11日 21时36分
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true, fluent = false)
public class MultiResponse<T> extends Response {
    private static final long serialVersionUID = -5246205708273877947L;

    /**
     * 响应数据，多个
     */
    private List<T> data;

    public static <T> MultiResponse<T> of(List<T> data) {
        MultiResponse<T> response = new MultiResponse<>();
        response.setCode(SUCCESS_CODE);
        response.setData(data);
        return response;
    }
}
