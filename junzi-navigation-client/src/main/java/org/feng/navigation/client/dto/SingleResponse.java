package org.feng.navigation.client.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 单个响应结果的响应
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年01月11日 21时20分
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true, fluent = false)
public class SingleResponse<T> extends Response {
    private static final long serialVersionUID = 4626934787555072114L;
    /**
     * 响应数据
     */
    private T data;



    public static <T> SingleResponse<T> of(T data) {
        SingleResponse<T> response = new SingleResponse<>();
        response.setCode(SUCCESS_CODE);
        response.setData(data);
        return response;
    }
}
