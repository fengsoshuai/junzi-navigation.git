package org.feng.navigation.client.dto.tianxing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.ApiDTO;

/**
 * 天行天气预报查询响应DTO
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 16时06分
 */
@Data
@ApiModel
public class TianXingWeatherSearchResponseDTO implements ApiDTO {
    /**
     * 错误描述
     */
    @ApiModelProperty(value = "错误描述")
    private String errorMsg;
    /**
     * 结果地区（市/区/县）
     */
    @ApiModelProperty(value = "结果地区（市/区/县）")
    private String area;
    /**
     * 城市天气ID
     */
    @ApiModelProperty(value = "城市天气ID")
    private String areaid;
    /**
     * 日期
     */
    @ApiModelProperty(value = "日期")
    private String date;
    /**
     * 最高温（日间温度）
     */
    @ApiModelProperty(value = "最高温（日间温度）")
    private String highest;
    /**
     * 最低温（夜间温度）
     */
    @ApiModelProperty(value = "最低温（夜间温度）")
    private String lowest;
    /**
     * 所属省份
     */
    @ApiModelProperty(value = "所属省份")
    private String province;
    /**
     * 空气质量提示（七天无此字段）
     */
    @ApiModelProperty(value = "空气质量提示（七天无此字段）")
    private String quality;
    /**
     * 实时气温（七天仅为参考）
     */
    @ApiModelProperty(value = "实时气温（七天仅为参考）")
    private String real;
    /**
     * 实时天气（七天为早晚变化）
     */
    @ApiModelProperty(value = "实时天气（七天为早晚变化）")
    private String weather;
    /**
     * 天气代码
     */
    @ApiModelProperty(value = "天气代码")
    private String weathercode;
    /**
     * 天气图标
     */
    @ApiModelProperty(value = "天气图标")
    private String weatherimg;
    /**
     * 星期
     */
    @ApiModelProperty(value = "星期")
    private String week;
    /**
     * 风向（方位）
     */
    @ApiModelProperty(value = "风向（方位）")
    private String wind;
}
