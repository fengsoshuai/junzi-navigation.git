package org.feng.navigation.client.dto.qyapi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.RequestDTO;

import javax.validation.constraints.NotNull;

/**
 * 企业微信机器人请求体DTO
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 16时09分
 */
@Data
@ApiModel
public class EnterpriseWechatRobotRequestDTO implements RequestDTO {
    private static final long serialVersionUID = -796066662159459237L;
    /**
     * 消息类型
     */
    @ApiModelProperty(value = "消息类型")
    private String messageType;
    /**
     * 消息内容
     */
    @ApiModelProperty(value = "消息内容")
    @NotNull
    private String content;

    @ApiModelProperty(value = "机器人的key的ID")
    @NotNull
    private Integer robotKey;

    public EnterpriseWechatRobotRequestDTO() {
        if (messageType == null) {
            messageType = "text";
        }
    }
}
