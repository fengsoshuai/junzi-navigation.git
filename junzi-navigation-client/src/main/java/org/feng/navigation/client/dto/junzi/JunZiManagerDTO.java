package org.feng.navigation.client.dto.junzi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.RequestDTO;

import java.time.LocalDateTime;

/**
 * 菌子管理者DTO
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月13日 15时07分
 */
@Data
@ApiModel
public class JunZiManagerDTO implements RequestDTO {

    private static final long serialVersionUID = -8150888048211504821L;
    @ApiModelProperty(value = "ID")
    private Integer junziId;
    @ApiModelProperty(value = "用户名")
    private String name;
    @ApiModelProperty(value = "登录名")
    private String loginName;
    @ApiModelProperty(value = "注册时间")
    private LocalDateTime createDatetime;
}
