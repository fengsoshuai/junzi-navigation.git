package org.feng.navigation.client.dto;

import java.io.Serializable;

/**
 * 所有的DTO应该实现该接口
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月11日 19时33分
 */
public interface ApiDTO extends Serializable {
}
