package org.feng.navigation.client.dto.qyapi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.ApiDTO;

/**
 * 企业微信机器人响应体DTO
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 16时10分
 */
@Data
@ApiModel
public class EnterpriseWechatRobotResponseDTO implements ApiDTO {
    private static final long serialVersionUID = 6989353782944636328L;
    @ApiModelProperty(value = "发送是否成功")
    private Boolean success;

    @ApiModelProperty(value = "错误信息")
    private String errorMessage;
}
