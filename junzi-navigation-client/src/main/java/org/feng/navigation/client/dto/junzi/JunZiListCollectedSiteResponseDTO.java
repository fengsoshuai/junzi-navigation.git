package org.feng.navigation.client.dto.junzi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.ApiDTO;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 菌子-查询站点信息的响应DTO
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时43分
 */
@Data
@ApiModel
public class JunZiListCollectedSiteResponseDTO implements ApiDTO {

    private static final long serialVersionUID = -5027814893077983164L;
    @ApiModelProperty("网站数据信息")
    private List<JunZiListCollectedSiteDataBody> collectedSiteDataBodyList;

    @Data
    public static class JunZiListCollectedSiteDataBody {
        @ApiModelProperty(value = "网站数据信息列表")
        private List<JunZiListCollectedSiteData> dataList;
        @ApiModelProperty(value = "网站类型")
        private String siteType;
    }

    @Data
    public static class JunZiListCollectedSiteData {
        @ApiModelProperty(value = "网站名")
        private String name;
        private transient String siteType;
        @ApiModelProperty(value = "网站URL")
        private String url;
        @ApiModelProperty(value = "网站ico")
        private String ico;
        @ApiModelProperty(value = "网站Logo")
        private String logo;
        @ApiModelProperty(value = "网站描述")
        private String remark;
        @ApiModelProperty(value = "网站收录时间")
        private LocalDateTime createdTime;
        @ApiModelProperty(value = "最近网站数据修正时间")
        private LocalDateTime updateTime;
    }
}
