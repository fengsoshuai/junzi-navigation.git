package org.feng.navigation.client.dto.tianxing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.RequestDTO;

/**
 * 天行新闻请求体
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 16时20分
 */
@Data
@ApiModel
public class TianXingAllNewsRequestDTO implements RequestDTO {
    private static final long serialVersionUID = -2732561959268836054L;

    @ApiModelProperty(value = "新闻个数")
    private Integer newsCount;
    @ApiModelProperty(value = "新闻频道ID")
    private Integer channelId;
}
