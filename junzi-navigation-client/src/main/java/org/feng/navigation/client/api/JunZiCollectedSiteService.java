package org.feng.navigation.client.api;

import org.feng.navigation.client.dto.junzi.JunZiAddCollectedSiteRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiAddCollectedSiteResponseDTO;
import org.feng.navigation.client.dto.junzi.JunZiListCollectedSiteRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiListCollectedSiteResponseDTO;

/**
 * 菌子站点服务接口
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时34分
 */
public interface JunZiCollectedSiteService {
    /**
     * 增加网站
     *
     * @param requestDTO 请求dto
     * @return 响应dto
     */
    JunZiAddCollectedSiteResponseDTO addCollectedSite(JunZiAddCollectedSiteRequestDTO requestDTO);

    /**
     * 分页列出网站
     *
     * @param requestDTO 请求dto
     * @return 响应dto
     */
    JunZiListCollectedSiteResponseDTO listCollectedSites(JunZiListCollectedSiteRequestDTO requestDTO);
}
