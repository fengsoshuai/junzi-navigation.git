package org.feng.navigation.client.api;

import org.feng.navigation.client.dto.generator.GeneratorCustomEnumRequestDTO;
import org.feng.navigation.client.dto.generator.GeneratorCustomEnumResponseDTO;

/**
 * 生成器服务
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 20时21分
 */
public interface GeneratorService {

    /**
     * 生成JAVA枚举
     *
     * @param requestDTO 请求参数
     * @return 响应结果，含类文件内容
     */
    GeneratorCustomEnumResponseDTO generatorCustomEnum(GeneratorCustomEnumRequestDTO requestDTO);

}
