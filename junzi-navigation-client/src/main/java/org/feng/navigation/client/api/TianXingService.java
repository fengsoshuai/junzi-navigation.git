package org.feng.navigation.client.api;

import org.feng.navigation.client.dto.tianxing.*;

/**
 * 天行数据-业务服务
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 16时19分
 */
public interface TianXingService {
    /**
     * 获取分类新闻
     *
     * @param requestDTO 请求体
     * @return 响应体
     */
    TianXingAllNewsResponseDTO allNews(TianXingAllNewsRequestDTO requestDTO);

    /**
     * 查询天气
     *
     * @param requestDTO 请求体
     * @return 响应体
     */
    TianXingWeatherSearchResponseDTO weatherSearch(TianXingWeatherSearchRequestDTO requestDTO);

    /**
     * 聊天机器人
     *
     * @param requestDTO 请求体
     * @return 响应体
     */
    TianXingRobotChatResponseDTO chatRobot(TianXingRobotChatRequestDTO requestDTO);

}
