package org.feng.navigation.client.dto.junzi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.RequestDTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 菌子清除缓存请求体
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月12日 15时09分
 */
@Data
@ApiModel
public class JunZiClearCacheRequestDTO implements RequestDTO {
    private static final long serialVersionUID = 6565230870102814156L;
    @ApiModelProperty(value = "缓存名")
    @NotBlank
    private String cacheName;
    @ApiModelProperty(value = "缓存key")
    @NotNull
    private Object key;
}
