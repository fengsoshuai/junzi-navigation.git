package org.feng.navigation.client.dto.tianxing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.ApiDTO;

/**
 * 聊天机器人响应体
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月03日 14时41分
 */
@Data
@ApiModel
public class TianXingRobotChatResponseDTO implements ApiDTO {
    /**
     * 回复内容
     */
    @ApiModelProperty(value = "回复内容")
    private String reply;
    /**
     * 错误描述
     */
    @ApiModelProperty(value = "错误描述")
    private String errorMsg;
}
