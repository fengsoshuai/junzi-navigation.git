package org.feng.navigation.client.dto.junzi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.feng.navigation.client.dto.RequestDTO;

/**
 * TODO 类的描述
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时34分
 */
@Data
@ApiModel
public class JunZiAddCollectedSiteRequestDTO implements RequestDTO {
    private static final long serialVersionUID = -2750262836332666352L;
    @ApiModelProperty(value = "网站名")
    private String name;
    @ApiModelProperty(value = "网站类型")
    private Integer siteType;
    @ApiModelProperty(value = "网站URL")
    private String url;
    @Setter(AccessLevel.NONE)
    private String ico;
    @ApiModelProperty(value = "网站Logo")
    private String logo;
    @ApiModelProperty(value = "网站描述")
    private String remark;
}
