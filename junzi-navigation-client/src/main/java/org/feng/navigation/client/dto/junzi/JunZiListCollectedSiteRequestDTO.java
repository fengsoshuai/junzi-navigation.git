package org.feng.navigation.client.dto.junzi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.RequestDTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * 菌子-查询站点信息请求体DTO
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时41分
 */
@Data
@ApiModel
public class JunZiListCollectedSiteRequestDTO implements RequestDTO {
    private static final long serialVersionUID = -2568944152997311398L;

    @ApiModelProperty(value = "当前页", required = true)
    @Max(value = Integer.MAX_VALUE)
    @Min(value = 1, message = "当前页不能小于1")
    private Integer currentPage;
    @ApiModelProperty(value = "每页大小", required = true)
    @Min(value = 1, message = "每页大小最小为1")
    @Max(value = Integer.MAX_VALUE)
    private Integer pageSize;
}
