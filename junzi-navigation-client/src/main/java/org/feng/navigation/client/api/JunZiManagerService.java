package org.feng.navigation.client.api;

import org.feng.navigation.client.dto.junzi.JunZiManagerDTO;

import java.util.List;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月13日 14时20分
 */
public interface JunZiManagerService {
   List<JunZiManagerDTO> listJunZiManager();
}
