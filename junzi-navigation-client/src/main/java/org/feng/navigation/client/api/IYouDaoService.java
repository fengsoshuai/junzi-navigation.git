package org.feng.navigation.client.api;

import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateRequestDTO;
import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateResponseDTO;

/**
 * 有道-业务服务
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月10日 10时38分
 */
public interface IYouDaoService {
    /**
     * 文本翻译
     *
     * @param youDaoTextTranslateRequestDTO 请求体
     * @return 响应体
     */
    YouDaoTextTranslateResponseDTO textTranslate(YouDaoTextTranslateRequestDTO youDaoTextTranslateRequestDTO);
}
