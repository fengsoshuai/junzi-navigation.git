package org.feng.navigation.client.dto.youdao;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.RequestDTO;

import javax.validation.constraints.NotNull;

/**
 * 有道文本翻译请求体DTO
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月18日 21时53分
 */
@Data
@ApiModel
public class YouDaoTextTranslateRequestDTO implements RequestDTO {
    private static final long serialVersionUID = 4780878896029369478L;
    @ApiModelProperty(value = "要翻译的文本的语言")
    private String from;
    @ApiModelProperty(value = "翻译结果的语言类型")
    @NotNull
    private String to;
    @ApiModelProperty(value = "要翻译的文本内容")
    @NotNull
    private String text;
}
