package org.feng.navigation.client.dto.junzi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.ApiDTO;

/**
 * TODO 类的描述
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时38分
 */
@Data
@ApiModel
public class JunZiAddCollectedSiteResponseDTO implements ApiDTO {
    private static final long serialVersionUID = -7569098763639630145L;
    @ApiModelProperty(value = "是否成功")
    private Boolean success;
}
