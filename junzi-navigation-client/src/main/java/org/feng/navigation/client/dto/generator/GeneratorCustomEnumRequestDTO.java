package org.feng.navigation.client.dto.generator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.RequestDTO;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 20时23分
 */
@Data
@ApiModel
public class GeneratorCustomEnumRequestDTO implements RequestDTO {
    private static final long serialVersionUID = 5422350583027510389L;
    @ApiModelProperty(value = "枚举内容")
    private String text;

    /**
     * 枚举类名
     */
    @ApiModelProperty(value = "枚举类名")
    private String customEnumClassName;

    /**
     * 完整包名
     */
    @ApiModelProperty(value = "完整包名")
    private String fullPackageName;
    /**
     * key值下标
     */
    @ApiModelProperty(value = "key值下标")
    private Integer keyIndex;
}
