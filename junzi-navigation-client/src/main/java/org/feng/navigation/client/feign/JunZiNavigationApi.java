package org.feng.navigation.client.feign;

import org.feng.navigation.client.dto.MultiResponse;
import org.feng.navigation.client.dto.SingleResponse;
import org.feng.navigation.client.dto.generator.GeneratorCustomEnumRequestDTO;
import org.feng.navigation.client.dto.generator.GeneratorCustomEnumResponseDTO;
import org.feng.navigation.client.dto.junzi.JunZiClearCacheRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiClearCacheResponseDTO;
import org.feng.navigation.client.dto.junzi.JunZiManagerDTO;
import org.feng.navigation.client.dto.tianxing.*;
import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateRequestDTO;
import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateResponseDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * 菌子导航Rest-API<br>
 * 用于Controller的实现，用于其他应用服务的Feign调用
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年2月1日 9时51分
 */
public interface JunZiNavigationApi {

    @PostMapping("/listJunZiManager")
    MultiResponse<JunZiManagerDTO> listJunZiManager();

    /**
     * 有道文本翻译
     *
     * @param requestDTO 请求体实例
     * @return 响应结果，翻译结果
     */
    @PostMapping("/textTranslate")
    SingleResponse<YouDaoTextTranslateResponseDTO> textTranslate(@RequestBody @Valid YouDaoTextTranslateRequestDTO requestDTO);

    /**
     * 天行数据-获取分类新闻
     *
     * @param requestDTO 请求体实例
     * @return 新闻列表响应体
     */
    @PostMapping("/listAllNews")
    SingleResponse<TianXingAllNewsResponseDTO> listAllNews(@RequestBody TianXingAllNewsRequestDTO requestDTO);

    /**
     * 天行数据-天气预报
     *
     * @param requestDTO 请求体实例
     * @return 天气预报响应体
     */
    @PostMapping("/listWeather")
    SingleResponse<TianXingWeatherSearchResponseDTO> listWeather(@RequestBody TianXingWeatherSearchRequestDTO requestDTO);

    @PostMapping("/chatRobot")
    SingleResponse<TianXingRobotChatResponseDTO> chatRobot(@RequestBody TianXingRobotChatRequestDTO requestDTO);

    /**
     * 生成自定义的java枚举
     *
     * @param requestDTO 请求体实例
     * @return 生成的枚举内容
     */
    @PostMapping("/generatorCustomEnum")
    SingleResponse<GeneratorCustomEnumResponseDTO> generatorCustomEnum(@RequestBody GeneratorCustomEnumRequestDTO requestDTO);

    /**
     * 清除内存缓存
     *
     * @param requestDTO 请求体实例
     * @return 清除是否成功
     */
    @PostMapping("/clearCache")
    SingleResponse<JunZiClearCacheResponseDTO> clearCache(@Valid @RequestBody JunZiClearCacheRequestDTO requestDTO);
}
