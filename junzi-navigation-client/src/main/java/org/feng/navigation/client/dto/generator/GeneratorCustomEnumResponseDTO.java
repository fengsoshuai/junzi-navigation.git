package org.feng.navigation.client.dto.generator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.ApiDTO;

/**
 * TODO 类的描述
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月07日 20时24分
 */
@Data
@ApiModel
public class GeneratorCustomEnumResponseDTO implements ApiDTO {
    @ApiModelProperty(value = "生成枚举类内容")
    private String generatedContent;
}
