package org.feng.navigation.client.api;

import org.feng.navigation.client.dto.junzi.JunZiClearCacheRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiClearCacheResponseDTO;

/**
 * 菌子缓存服务
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月12日 15时08分
 */
public interface JunZiCacheService {
    JunZiClearCacheResponseDTO clearCache(JunZiClearCacheRequestDTO requestDTO);
}
