package org.feng.navigation.client.dto;

/**
 * 参数检查接口：校验参数，不合法时抛出异常
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月11日 19时33分
 */
public interface Check {
    /**
     * 校验参数：当前DTO需要校验参数时，重写这个方法即可。最终通过切面来前置调用
     *
     * @throws IllegalArgumentException 参数校验失败时，抛出异常
     */
    default void check() throws IllegalArgumentException {
    }
}
