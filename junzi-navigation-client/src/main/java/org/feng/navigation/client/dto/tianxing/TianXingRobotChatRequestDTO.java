package org.feng.navigation.client.dto.tianxing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.RequestDTO;

/**
 * 聊天机器人请求体
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年03月03日 14时41分
 */
@Data
@ApiModel
public class TianXingRobotChatRequestDTO implements RequestDTO {
    @ApiModelProperty(value = "唯一ID")
    private String uniqueId;
    @ApiModelProperty(value = "问题内容")
    private String question;
}
