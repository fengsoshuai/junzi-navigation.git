package org.feng.navigation.client.dto;

/**
 * 响应体DTO：所有响应体应该实现它
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年02月01日 09时05分
 */
public interface ResponseDTO extends ApiDTO {
}
