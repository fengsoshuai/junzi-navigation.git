package org.feng.navigation.client.dto;

/**
 * 常量池
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年01月11日 21时25分
 */
public interface Constant {
    /**
     * 英文逗号
     */
    String ENGLISH_COMMA = ",";
}
