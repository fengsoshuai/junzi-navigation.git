package org.feng.navigation.client.dto.tianxing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.ApiDTO;

import java.time.LocalDate;
import java.util.List;

/**
 * 天行新闻响应体
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年02月05日 16时21分
 */
@Data
@ApiModel
public class TianXingAllNewsResponseDTO implements ApiDTO {
    private static final long serialVersionUID = -6311211912823802681L;
    @ApiModelProperty(value = "新闻列表")
    private List<TianXingAllNewsData> newsList;

    /**
     * 错误描述
     */
    @ApiModelProperty(value = "错误描述")
    private String errorMsg;

    @Data
    @ApiModel
    public static class TianXingAllNewsData {
        /**
         * 时间
         */
        @ApiModelProperty(value = "时间")
        private LocalDate date;
        /**
         * 标题
         */
        @ApiModelProperty(value = "标题")
        private String title;
        /**
         * 新闻来源
         */
        @ApiModelProperty(value = "新闻来源")
        private String source;
        /**
         * 配图地址
         */
        @ApiModelProperty(value = "配图地址")
        private String picUrl;
        /**
         * 新闻地址
         */
        @ApiModelProperty(value = "新闻地址")
        private String url;
    }
}
