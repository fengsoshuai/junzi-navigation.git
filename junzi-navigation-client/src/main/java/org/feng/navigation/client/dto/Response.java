package org.feng.navigation.client.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 响应
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年01月11日 21时12分
 */
@Data
@Accessors(chain = true, fluent = false)
public class Response implements Serializable {
    /**
     * 成功响应码：0
     */
    public static final Integer SUCCESS_CODE = 0;
    private static final long serialVersionUID = 130196817456054710L;

    /**
     * 状态码
     */
    private Integer code;
    /**
     * 响应描述信息
     */
    private String message;

    /**
     * 响应成功
     *
     * @return true 表示本次请求是成功的，返回的是成功的响应结果；false表示失败
     */
    public boolean isSuccess() {
        return SUCCESS_CODE.equals(code);
    }

    /**
     * 构建一个错误的响应实体
     *
     * @param error       错误内容
     * @param errorDetail 错误详情
     * @return 响应实体
     */
    public static Response buildFailure(IError error, String errorDetail) {
        Response response = new Response();
        response.setCode(error.getErrorCode());
        response.setMessage(error.getErrorMessage() + Constant.ENGLISH_COMMA + " " + errorDetail);
        return response;
    }
}
