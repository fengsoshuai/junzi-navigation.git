package org.feng.navigation.client.dto;

/**
 * 错误
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年01月11日 21时28分
 */
public interface IError {
    /**
     * 获取错误码
     *
     * @return 错误码
     */
    Integer getErrorCode();

    /**
     * 获取错误信息
     *
     * @return 错误信息描述
     */
    String getErrorMessage();
}
