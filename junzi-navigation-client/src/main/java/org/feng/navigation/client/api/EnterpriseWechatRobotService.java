package org.feng.navigation.client.api;

import org.feng.navigation.client.dto.qyapi.EnterpriseWechatRobotRequestDTO;
import org.feng.navigation.client.dto.qyapi.EnterpriseWechatRobotResponseDTO;

/**
 * 企业微信机器人服务
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 15时57分
 */
public interface EnterpriseWechatRobotService {

    EnterpriseWechatRobotResponseDTO robotSend(EnterpriseWechatRobotRequestDTO requestDTO);
}
