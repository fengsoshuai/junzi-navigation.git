package org.feng.navigation.client.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 错误码-错误信息枚举
 *
 * @version v1.0
 * @author: junzi
 * @date: 2023年01月11日 21时28分
 */
@Getter
@AllArgsConstructor
public enum ErrorEnum implements IError {

    /**
     * 参数校验错误
     */
    METHOD_ARGUMENTS_VALID_ERROR(20000, "参数校验错误"),
    APPLICATION_ERROR(30000, "服务器错误"),
    /**
     * 业务错误
     */
    BUSSINESS_ERROR(40000, "业务错误");

    /**
     * 错误码
     */
    private final Integer errorCode;
    /**
     * 错误信息描述
     */
    private final String errorMessage;
}
