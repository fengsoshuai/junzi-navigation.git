package org.feng.navigation.client.dto.junzi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.feng.navigation.client.dto.ApiDTO;

/**
 * 菌子清除缓存响应体
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月12日 15时13分
 */
@Data
@ApiModel
public class JunZiClearCacheResponseDTO implements ApiDTO {
    private static final long serialVersionUID = -6504679085013147398L;
    @ApiModelProperty(value = "是否清除成功")
    private Boolean success;
}
