package org.feng.navigation.client.dto.tianxing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 天行-天气查询请求DTO
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月19日 16时06分
 */
@Data
@ApiModel
public class TianXingWeatherSearchRequestDTO {
    /**
     * 省
     */
    @ApiModelProperty(value = "省", example = "陕西")
    private String province;
    /**
     * 地区
     */
    @ApiModelProperty(value = "地区", example = "西安")
    private String area;
}
