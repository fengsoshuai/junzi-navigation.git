package org.feng.navigation.adapter.filter;

import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.feng.navigation.common.util.GsonUtil;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Objects;


/**
 * 对请求进行过滤，输出请求URI、参数、响应、调用耗时等
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月27日 14时31分
 */
@Slf4j
public class HttpRequestFilter extends OncePerRequestFilter implements Ordered {
    /**
     * 需要过滤器处理的路径
     */
    private static final String NEED_TRACE_PATH_PREFIX = "/";
    /**
     * 忽略的内容类型
     */
    private static final String IGNORE_CONTENT_TYPE = "multipart/form-data";

    /**
     * 设置优先级为最低
     *
     * @return 优先级值
     */
    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain filterChain) throws ServletException, IOException {
        // 不是一个请求Uri
        if (!isRequestUri(request)) {
            filterChain.doFilter(request, response);
            return;
        }
        if (!(request instanceof ContentCachingRequestWrapper)) {
            request = new ContentCachingRequestWrapper(request);
        }
        if (!(response instanceof ContentCachingResponseWrapper)) {
            response = new ContentCachingResponseWrapper(response);
            // 修正默认编码
            if (response.getCharacterEncoding() == null || response.getCharacterEncoding().equals(StandardCharsets.ISO_8859_1.name())) {
                response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            }
        }
        // 响应状态：初始为错误，之后使用响应覆盖该值
        int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
        long startTime = System.currentTimeMillis();
        try {
            filterChain.doFilter(request, response);
            responseStatus = response.getStatus();
        } finally {
            String path = request.getRequestURI();
            if (path.startsWith(NEED_TRACE_PATH_PREFIX) && !Objects.equals(IGNORE_CONTENT_TYPE, request.getContentType())) {
                HttpLog httpLog = new HttpLog();
                httpLog.setPath(path);
                httpLog.setMethod(request.getMethod());
                long latency = System.currentTimeMillis() - startTime;
                httpLog.setTimeTaken(latency);
                httpLog.setTime(LocalDateTime.now().toString());
                httpLog.setParameterMap(GsonUtil.toJson(request.getParameterMap()));
                httpLog.setStatus(responseStatus);
                httpLog.setRequestBody(getRequestBody(request));
                httpLog.setResponseBody(getResponseBody(response));
                log.info("请求路径={}, 状态码:{}, 耗时毫秒:{}, 请求报文:{}, 响应报文:{}", httpLog.getPath(), httpLog.getStatus(), httpLog.getTimeTaken(), httpLog.getRequestBody(), httpLog.getResponseBody());
            }
            updateResponse(response);
        }
    }

    private boolean isRequestUri(HttpServletRequest request) {
        try {
            new URI(request.getRequestURL().toString());
            return true;
        } catch (URISyntaxException ex) {
            return false;
        }
    }

    private String getRequestBody(HttpServletRequest request) throws IOException {
        String requestBody = "";
        ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        if (wrapper != null) {
            requestBody = IOUtils.toString(wrapper.getContentAsByteArray(), wrapper.getCharacterEncoding());
        }
        return requestBody;
    }

    private String getResponseBody(HttpServletResponse response) throws IOException {
        String responseBody = "";
        ContentCachingResponseWrapper wrapper = WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
        if (wrapper != null) {
            responseBody = IOUtils.toString(wrapper.getContentAsByteArray(), wrapper.getCharacterEncoding());
        }
        return responseBody;
    }

    private void updateResponse(HttpServletResponse response) throws IOException {
        ContentCachingResponseWrapper responseWrapper = WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
        Objects.requireNonNull(responseWrapper).copyBodyToResponse();
    }

    @Data
    private static class HttpLog {
        private String path;
        private String parameterMap;
        private String method;
        private Long timeTaken;
        private String time;
        private Integer status;
        private String requestBody;
        private String responseBody;
    }
}
