package org.feng.navigation.adapter.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.client.api.EnterpriseWechatRobotService;
import org.feng.navigation.client.dto.Response;
import org.feng.navigation.client.dto.SingleResponse;
import org.feng.navigation.client.dto.qyapi.EnterpriseWechatRobotRequestDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 菌子企业微信机器人控制器
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年04月11日 16时30分
 */
@Api(tags = "菌子企业微信机器人控制器")
@Slf4j
@RestController
@RequestMapping("/qyapi")
public class JunZiEnterpriseWechatRobotController {
    @Resource
    private EnterpriseWechatRobotService enterpriseWechatRobotService;

    @ApiOperation("机器人发送消息")
    @PostMapping("/robotSend")
    public Response robotSend(@RequestBody EnterpriseWechatRobotRequestDTO requestDTO) {
        return SingleResponse.of(enterpriseWechatRobotService.robotSend(requestDTO));
    }
}
