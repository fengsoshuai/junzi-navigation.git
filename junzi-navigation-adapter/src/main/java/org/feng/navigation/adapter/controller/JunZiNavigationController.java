package org.feng.navigation.adapter.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.client.api.*;
import org.feng.navigation.client.dto.MultiResponse;
import org.feng.navigation.client.dto.SingleResponse;
import org.feng.navigation.client.dto.generator.GeneratorCustomEnumRequestDTO;
import org.feng.navigation.client.dto.generator.GeneratorCustomEnumResponseDTO;
import org.feng.navigation.client.dto.junzi.JunZiClearCacheRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiClearCacheResponseDTO;
import org.feng.navigation.client.dto.junzi.JunZiManagerDTO;
import org.feng.navigation.client.dto.tianxing.*;
import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateRequestDTO;
import org.feng.navigation.client.dto.youdao.YouDaoTextTranslateResponseDTO;
import org.feng.navigation.client.feign.JunZiNavigationApi;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 菌子导航控制器
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月10日 11时44分
 */
@Api(tags = "菌子导航控制器")
@Slf4j
@RestController
public class JunZiNavigationController implements JunZiNavigationApi {

    @Resource
    private IYouDaoService iYouDaoService;

    @Resource
    private JunZiManagerService junZiManagerService;

    @Resource
    private TianXingService tianXingService;

    @Resource
    private GeneratorService generatorService;

    @Resource
    private JunZiCacheService junZiCacheService;

    @ApiOperation(value = "测试-列出 junzi_manager 表的数据", notes = "测试")
    @Override
    public MultiResponse<JunZiManagerDTO> listJunZiManager() {
        List<JunZiManagerDTO> junZiManagerList = junZiManagerService.listJunZiManager();
        return MultiResponse.of(junZiManagerList);
    }

    @ApiOperation(value = "有道文本翻译", notes = "有道文本翻译")
    @Override
    public SingleResponse<YouDaoTextTranslateResponseDTO> textTranslate(YouDaoTextTranslateRequestDTO requestDTO) {
        return SingleResponse.of(iYouDaoService.textTranslate(requestDTO));
    }

    @ApiOperation(value = "天行数据-获取分类新闻")
    public SingleResponse<TianXingAllNewsResponseDTO> listAllNews(TianXingAllNewsRequestDTO requestDTO) {
        TianXingAllNewsResponseDTO responseDTO = tianXingService.allNews(requestDTO);
        return SingleResponse.of(responseDTO);
    }

    @ApiOperation(value = "天行数据-天气预报")
    @Override
    public SingleResponse<TianXingWeatherSearchResponseDTO> listWeather(TianXingWeatherSearchRequestDTO requestDTO) {
        TianXingWeatherSearchResponseDTO responseDTO = tianXingService.weatherSearch(requestDTO);
        return SingleResponse.of(responseDTO);
    }

    @ApiOperation(value = "天行数据-聊天机器人")
    @Override
    public SingleResponse<TianXingRobotChatResponseDTO> chatRobot(TianXingRobotChatRequestDTO requestDTO) {
        TianXingRobotChatResponseDTO responseDTO = tianXingService.chatRobot(requestDTO);
        return SingleResponse.of(responseDTO);
    }

    @ApiOperation(value = "生成JAVA枚举")
    @Override
    public SingleResponse<GeneratorCustomEnumResponseDTO> generatorCustomEnum(GeneratorCustomEnumRequestDTO requestDTO) {
        GeneratorCustomEnumResponseDTO responseDTO = generatorService.generatorCustomEnum(requestDTO);
        return SingleResponse.of(responseDTO);
    }

    @ApiOperation(value = "清除缓存")
    @Override
    public SingleResponse<JunZiClearCacheResponseDTO> clearCache(JunZiClearCacheRequestDTO requestDTO) {
        JunZiClearCacheResponseDTO responseDTO = junZiCacheService.clearCache(requestDTO);
        return SingleResponse.of(responseDTO);
    }
}
