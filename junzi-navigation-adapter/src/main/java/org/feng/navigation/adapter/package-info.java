/**
 * 适配层：对外提供api接口，集成swagger，同一日志处理的切面，全局异常拦截等
 *
 * @author junzi
 */
package org.feng.navigation.adapter;