package org.feng.navigation.adapter.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.feng.navigation.client.api.JunZiCollectedSiteService;
import org.feng.navigation.client.dto.Response;
import org.feng.navigation.client.dto.SingleResponse;
import org.feng.navigation.client.dto.junzi.JunZiAddCollectedSiteRequestDTO;
import org.feng.navigation.client.dto.junzi.JunZiListCollectedSiteRequestDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 菌子-站点控制器
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2023年02月16日 21时59分
 */
@Api(tags = "站点控制器")
@Slf4j
@RestController
@RequestMapping("/sites")
public class JunZiCollectedSiteController {

    @Resource
    private JunZiCollectedSiteService junZiCollectedSiteService;

    @ApiOperation("增加网站信息")
    @PostMapping("/add")
    public Response addSite(@RequestBody JunZiAddCollectedSiteRequestDTO requestDTO) {
        return SingleResponse.of(junZiCollectedSiteService.addCollectedSite(requestDTO));
    }

    @ApiOperation("查询网站信息")
    @PostMapping("/list")
    public Response listSite(@RequestBody @Valid JunZiListCollectedSiteRequestDTO requestDTO) {
        return SingleResponse.of(junZiCollectedSiteService.listCollectedSites(requestDTO));
    }
}
