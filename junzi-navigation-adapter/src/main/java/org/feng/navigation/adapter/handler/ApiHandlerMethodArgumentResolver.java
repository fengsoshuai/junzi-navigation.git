package org.feng.navigation.adapter.handler;

import lombok.NonNull;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


/**
 * 禁用API方法的参数解析器
 * <br> TODO 待确定具体业务功能
 * https://blog.csdn.net/qq_35387940/article/details/128566792
 * https://blog.csdn.net/qq_36408229/article/details/104436508
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月11日 19时07分
 */
public class ApiHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
    /**
     * 判断Controller层中的参数，是否满足条件，满足条件则执行resolveArgument方法，不满足则跳过
     *
     * @param parameter api参数
     * @return true 表示执行resolveArgument；false表示不执行resolveArgument
     */
    @Override
    public boolean supportsParameter(@NonNull MethodParameter parameter) {
        return false;
    }

    @Override
    public Object resolveArgument(@NonNull MethodParameter parameter, ModelAndViewContainer mavContainer, @NonNull NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        return null;
    }
}
