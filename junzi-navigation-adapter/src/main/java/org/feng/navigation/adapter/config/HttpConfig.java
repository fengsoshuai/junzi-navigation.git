package org.feng.navigation.adapter.config;

import org.feng.navigation.adapter.filter.HttpRequestFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Http的配置
 *
 * @version V1.0
 * @author: fengjinsong
 * @date: 2023年02月27日 14时55分
 */
@Configuration
@ConditionalOnWebApplication
public class HttpConfig {
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    public static class ServletTraceFilterConfiguration {
        /**
         * 请求过滤器：输出请求、响应、耗时、http状态
         *
         * @return {@link HttpRequestFilter} 实例
         */
        @Bean
        public HttpRequestFilter httpTraceLogFilter() {
            return new HttpRequestFilter();
        }
    }
}
