package org.feng.navigation.adapter.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 日志切面
 *
 * @version V1.0
 * @author: junzi
 * @date: 2023年01月12日 10时13分
 */
@Slf4j
@Aspect
@Order(1)
@Component
public class LogAspect {
    @Pointcut("execution(public * org.feng.navigation.adapter.controller.*.*(..))")
    private void apiMethod() {
    }

    @Before(value = "apiMethod()")
    public void before(JoinPoint joinPoint) {
        String method = joinPoint.getSignature().getName();
        MDC.put("method", method);
    }

    @After(value = "apiMethod()")
    public void after(JoinPoint joinPoint) {
        MDC.remove("method");
    }
}
