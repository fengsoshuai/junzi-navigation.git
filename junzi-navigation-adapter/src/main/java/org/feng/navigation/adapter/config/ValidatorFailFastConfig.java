package org.feng.navigation.adapter.config;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * 校验器配置：设置快速失败
 *
 * @version V1.0
 * @author: junzi
 * @date: 2022年04月14日 18时22分
 */
@Configuration
public class ValidatorFailFastConfig {

    /**
     * 快速失败 (Fail Fast) <br>
     * Spring Validation 默认会校验完所有字段，然后才抛出异常。
     *
     * @return Validator
     */
    @Bean
    public Validator validator() {
        try (ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)
                .buildValidatorFactory()) {
            return validatorFactory.getValidator();
        }
    }
}
